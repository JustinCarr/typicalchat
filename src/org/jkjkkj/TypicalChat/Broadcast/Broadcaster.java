package org.jkjkkj.TypicalChat.Broadcast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.universicraft_mp.TypicalChat.PermManager;
import com.universicraft_mp.TypicalChat.TypicalChat;
import com.universicraft_mp.TypicalChat.UsefulUtils;
import com.universicraft_mp.TypicalChat.Vars;

public class Broadcaster implements CommandExecutor {
	
	private static TypicalChat plugin;
	private static File B_File;
	private static FileConfiguration B_Config;
	private static long Interval = 0;
	private static int tasknum = -1;
	private static ArrayList<String> broadcasts = new ArrayList<String>();
	private ArrayList<String> helpmsg = new ArrayList<String>();
	private static int index = 0;
	private String r = ChatColor.RED + "";
	private String w = ChatColor.WHITE + "";
	
	public Broadcaster(TypicalChat local){
		plugin = local;
		
		InitializeBroadcastsConfig();
		ReloadConfig();
		setValues();
		
		
		
		helpmsg.add(r + "==================================================");
		helpmsg.add(ChatColor.GRAY + "Here's how to use the '/tcb' command...");
		helpmsg.add(ChatColor.GRAY + "NOTICE: To reload broadcasts, type '/TypicalChat'");
		helpmsg.add(r + "/tcb help " + w + "View this help message");
		helpmsg.add(r + "/tcb enable/disable " + w + "Enable or disable broadcasts");
		helpmsg.add(r + "/tcb info " + w + "Get info on broadcasts");
		helpmsg.add(r + "/tcb list " + w + "List all current broadcasts");
		helpmsg.add(r + "/tcb clear " + w + "Clear all current broadcasts");
		helpmsg.add(r + "/tcb add MESSAGE " + w + "Add a message to be broadcasted");
		helpmsg.add(r + "==================================================");
		
		if (getEnabled()){
			
			scheduleTheTask();
		}
		
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
			public void run(){
				DoBroadcast();
			}
		}, 200L, (Vars.BROADCAST_INTERVAL * 1200L));
		
	}
	
	private static void DoBroadcast(){
		
		ArrayList<String> tosend = new ArrayList<String>(Vars.BROADCAST_MESSAGE.size());
		
		ArrayList<Player> players = new ArrayList<Player>();
		
		try {
			for (Player p : Bukkit.getServer().getOnlinePlayers()){
				players.add(p);
			}
		} catch(Exception e){
			
		}
		
		int online = players.size();
		int max = Bukkit.getMaxPlayers();
		int staffonline = UsefulUtils.getStaff().size();
		
		for (String s : Vars.BROADCAST_MESSAGE){
			tosend.add(s.replaceAll("%ONLINE%", online + "").replaceAll("%MAX%", max + "").replaceAll("%STAFF%", staffonline + ""));

		}
		
		for (Player p : players){
			for (String s : tosend){
				p.sendMessage(s);
			}
		}
	}
	
	private void scheduleTheTask(){
		
		tasknum = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
			
			public void run(){
				
				if (!getEnabled()){
					int i = this.getTaskId();
					plugin.getServer().getScheduler().cancelTask(i);
					tasknum = -1;
					this.cancel();
					return;
				}
				
				if (broadcasts.isEmpty()){
					return;
				}
				String tobroadcast = null;
				try {
					
					index = index + 1;
					tobroadcast = broadcasts.get(index);
				} catch (IndexOutOfBoundsException e){
					index = 0;
					tobroadcast = broadcasts.get(index);
				}
				
				Player[] players = plugin.getServer().getOnlinePlayers();
				for (Player p : players){
					
					if (Vars.chatIsOn(p.getUniqueId())){
						p.sendMessage(tobroadcast);
					}
					
				}
				return;
				
			}
			
		}, 20L, Interval);
		
	}
	
	private void InitializeBroadcastsConfig(){
		
		try {
			if(B_File == null){
				B_File = new File(plugin.getDataFolder(), "broadcasts.yml");
			}
			if(!B_File.exists()){
				plugin.saveResource("broadcasts.yml", false);
			}
			
		} catch (Exception e){
			TypicalChat.LogMessage("Couldn't copy broadcasts.yml to data folder.", true);
			e.printStackTrace();
			return;
		}
		
		B_Config = YamlConfiguration.loadConfiguration(B_File);
		Reader reader = null;
		
		try {
            reader = new InputStreamReader(plugin.getResource("broadcasts.yml"), "UTF8");
        } catch (UnsupportedEncodingException e) {
			TypicalChat.LogMessage("There was an encoding problem. Illegal character in config?", true);
			e.printStackTrace();
        }
 
        if (reader != null) {
            B_Config.setDefaults(YamlConfiguration.loadConfiguration(reader));
        }
		
	}
	
	private static void SaveConfigFile(){
		
		if (B_File == null || B_Config == null){
			TypicalChat.LogMessage("Saving has been haulted. File not found.", true);
			return;
		}
		try {
			B_Config.save(B_File);
		} catch (IOException e){
			TypicalChat.LogMessage("Something happened while saving the broadcasts data...", true);
			e.printStackTrace();
		} catch (IllegalArgumentException e){
			TypicalChat.LogMessage("Something happened while saving the broadcasts data...", true);
			e.printStackTrace();
		}
		
		
	}


	
	public static void reSetUpBroadcasts(){
		StoreToHardFile();
		InitializeBroadcastsConfig();
		CopyValues();
	}
	
	
	private static void CopyValues(){
		
		Interval = (B_Config.getInt("minutes_between_broadcasts")) * 60 * 20;

		for (String s : B_Config.getStringList("broadcasts")){
			
			broadcasts.add(ChatColor.translateAlternateColorCodes('&', s));
			
		}
		
		should save?
		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equals("tcb")){
			
			if (sender.hasPermission(PermManager.getPermNode("tcb"))){
				
				if (args.length == 0){
					
					for (String s : helpmsg){
						sender.sendMessage(s);
					}
					
				} else if (args[0].equalsIgnoreCase("help")){
					
					for (String s : helpmsg){
						sender.sendMessage(s);
					}
					
				} else if (args[0].equalsIgnoreCase("enable") || args[0].equalsIgnoreCase("on")){
					
					boolean taskexists = false;
					boolean rightinconfig = B_Config.getBoolean("enabled");
					if (tasknum != -1){
						if (plugin.getServer().getScheduler().isQueued(tasknum)){
							taskexists = true;
						}
					}
					
					if (taskexists && rightinconfig){
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Broadcasts are already " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
						
						return true;
					}
					
					B_Config.set("enabled", Boolean.valueOf(true));
					SaveConfig();
					if (!taskexists){
						scheduleTheTask();
					}
					
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Broadcasts are now " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
					
				} else if (args[0].equalsIgnoreCase("disable") || args[0].equalsIgnoreCase("off")){
					
					boolean taskexists = false;
					boolean enabledinconfig = B_Config.getBoolean("enabled");
					if (tasknum != -1){
						if (plugin.getServer().getScheduler().isQueued(tasknum)){
							taskexists = true;
						}
					}
					
					if (!taskexists && !enabledinconfig){
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Broadcasts are already " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
						
						return true;
					}
					
					B_Config.set("enabled", Boolean.valueOf(false));
					SaveConfig();
					if (taskexists){
						plugin.getServer().getScheduler().cancelTask(tasknum);
					}
					tasknum = -1;
					
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Broadcasts are now " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
					
				} else if (args[0].equalsIgnoreCase("info")){
					
					int size = broadcasts.size();
					int interval = B_Config.getInt("minutes_between_broadcasts");
					
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "There are " + Vars.Pos + size + Vars.Ntrl + " broadcasts that run every " + Vars.Pos + interval + Vars.Ntrl + " minutes!");
					
				} else if (args[0].equalsIgnoreCase("list")){
					
					sender.sendMessage(r + "==================================================");
					sender.sendMessage(w + "Below are all broadcasts in the system...");
					for (String s : broadcasts){
						sender.sendMessage(r + " - " + w + s);
					}
					sender.sendMessage(r + "==================================================");

				} else if (args[0].equalsIgnoreCase("clear")){
					
					broadcasts.clear();
					List<String> c_broadcasts = B_Config.getStringList("broadcasts");
					c_broadcasts.clear();
					B_Config.set("broadcasts", c_broadcasts);
					SaveConfig();
					
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "All broadcasts have been " + Vars.Neg + "CLEARED" + Vars.Ntrl + "!");
					
				} else if (args[0].equalsIgnoreCase("add")){
					
					if (args.length <= 1){
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/tcb add BROADCAST_MESSAGE");
						return true;
					}
					
					StringBuilder sb = new StringBuilder();
					for (int i =  1; i < args.length; i++){
						
						sb.append(args[i]).append(" ");
						
					}
					
					List<String> c_broadcasts = B_Config.getStringList("broadcasts");
					c_broadcasts.add(sb.toString());
					B_Config.set("broadcasts", c_broadcasts);
					SaveConfig();
					setValues();
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "A broadcast has been added to the system!");
					
				} else {
					
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. For help: " + Vars.Ntrl + "/tcb help");
					
				}
				
			}
			
			return true;
		}
		
		return false;
	}
}
