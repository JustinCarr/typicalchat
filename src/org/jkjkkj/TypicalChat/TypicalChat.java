package org.jkjkkj.TypicalChat;


import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.jkjkkj.TypicalChat.Broadcast.Broadcaster;
import org.jkjkkj.TypicalChat.ChatChannels.ChatHandler;
import org.jkjkkj.TypicalChat.Commands.TCCommands;

public class TypicalChat extends JavaPlugin {
	
	private static Logger log = Logger.getLogger("Minecraft");
	
	@Override
	public void onEnable(){
		
		new Vars(this);
		new TCCommands(this);
		PermManager.SetUpPerms(this);
		getServer().getPluginManager().registerEvents(new JoinLeaveHandler(this), this);
		getServer().getPluginManager().registerEvents(new DeathHandler(), this);
		getServer().getPluginManager().registerEvents(new ChatHandler(this), this);
		getCommand("tcb").setExecutor(new Broadcaster(this));
		
		if (isEssentialsThere()){
			LogMessage("Hooked with essentials!", false);
		} else {
			LogMessage("Essentials not found! Some things won't be checked.", true);
		}
		
	}
	
	@Override
	public void onDisable(){
		Vars.Nullify();
	}
	
	
	public static void LogMessage(String tolog, Boolean IsSevere){
		if (IsSevere){
			log.severe("[TypicalChat] " + tolog);
		} else {
			log.info("[TypicalChat] " + tolog);
		}
	}
	
	public static boolean isEssentialsThere(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("Essentials") != null){
			return true;
		} else {return false;}		
	}
	
	public static boolean isEssentialsEnabled(){
		
		if (Bukkit.getServer().getPluginManager().getPlugin("Essentials") == null){
			return false;
		}	
		
		return Bukkit.getServer().getPluginManager().isPluginEnabled("Essentials");	
	}
	
}
