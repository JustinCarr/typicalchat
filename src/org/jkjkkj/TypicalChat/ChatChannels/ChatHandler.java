package org.jkjkkj.TypicalChat.ChatChannels;

import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.TypicalChat;
import org.jkjkkj.TypicalChat.UsefulUtils;
import org.jkjkkj.TypicalChat.Vars;

public class ChatHandler implements Listener {

	private TypicalChat plugin;
	private String b = ChatColor.AQUA + "";
	private String g = ChatColor.GOLD + "";
	private String w = ChatColor.WHITE + "";
	
	public ChatHandler(TypicalChat instance){
		this.plugin = instance;	
		new Channels(this.plugin);
	}
	
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onTalk(AsyncPlayerChatEvent e){
		
		if (UsefulUtils.isMuted(e.getPlayer().getName())){
			
			e.getPlayer().sendMessage(Vars.NO_CHAT_MUTED);
			e.setCancelled(true);
			return;
			
		} else if (!Vars.CHAT_ENABLED){
			
			if (!e.getPlayer().hasPermission(PermManager.getPerm("chatlockbypass"))){
				
				e.getPlayer().sendMessage(Vars.NO_TALKING);
				e.getRecipients().clear();
				e.setMessage(null);
				e.setCancelled(true);
				return;
				
			}
		} else if (!Vars.chatIsOn(e.getPlayer().getUniqueId())) {
		
			e.getPlayer().sendMessage(Vars.Ntrl + "Oops. You cannot talk because you are in silent mode.");
			e.getPlayer().sendMessage(Vars.Ntrl + "Disable silentmode by: " + Vars.Neg + "/silentchat off");
			e.setMessage(null);
			e.getRecipients().clear();
			e.setCancelled(true);
			return;
		
		} else if (Vars.USE_SWEARS.contains(e.getPlayer().getName())){
			
			e.getPlayer().sendMessage(Vars.NO_TALKING_SWORE);
			e.getRecipients().clear();
			e.setMessage(null);
			e.setCancelled(true);
			return;
			
		}
		
		Player p = (Player) e.getPlayer();
		String msg = e.getMessage();
		String nono = UsefulUtils.GetWordsNotAllowed(p, e.getMessage());
		
		if (nono != null){
			
			p.sendMessage(Vars.Ntrl + "Oops. You cannot use the word '" + Vars.Neg + nono + Vars.Ntrl + "' in messages!");
			p.sendMessage(Vars.Ntrl + "You have been muted for " + Vars.Neg + 10 + Vars.Ntrl + " seconds.");
			Vars.USE_SWEARS.add(p.getName());
			final String n = p.getName();
			
			this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new BukkitRunnable(){
				public void run(){
					Vars.USE_SWEARS.remove(n);
				}
			}, 200L);
			
			e.setCancelled(true);
			return;
		
		}
		
		String pname = "";
		
		if (Channels.NAME_TYPE == 3){
			pname = UsefulUtils.GetNameWithPrefix(p, true);
			
		} else if (Channels.NAME_TYPE == 2){
			pname = UsefulUtils.GetNameWithColorsOnly(p, true);
			
		} else {
			pname = p.getName();
			
		}

		String print_to = "";
		
		if (msg.startsWith("#")){
			
			String[] temp = e.getMessage().split(" ", 2);
			
			if (temp.length != 2){
				
				e.setCancelled(true);
				p.sendMessage(Vars.Ntrl + "Oops. You need to include a message!");
				return;
				
			}
			
			msg = temp[1];
			
			if (temp[0].equalsIgnoreCase("#public") || temp[0].equalsIgnoreCase("#tc")){
				
				print_to = "public";
				
			} else if (temp[0].equalsIgnoreCase("#staff") || temp[0].equalsIgnoreCase("#s")){
				
				if (Channels.channelContainsPlayer("staff", p.getName())){
					
					print_to = "staff";
					
				} else if (p.hasPermission(PermManager.getPerm("chan-staff"))){
					
					DoCheckForPerms(p);
					print_to = "staff";
					
				} else {
					
					e.setCancelled(true);
					p.sendMessage(Vars.Ntrl + "Oops. You don't have permission to speak in that channel!");
					return;
					
				}
				
			} else if (temp[0].equalsIgnoreCase("#vip") || temp[0].equalsIgnoreCase("#v") || temp[0].equalsIgnoreCase("#donor")){
				
				if (Channels.channelContainsPlayer("vip", p.getName())){
					print_to = "vip";
					
				} else if (p.hasPermission(PermManager.getPerm("chan-vip"))){
					
					DoCheckForPerms(p);
					print_to = "vip";
					
				} else {
					
					e.setCancelled(true);
					p.sendMessage(Vars.Ntrl + "Oops. You don't have permission to speak in that channel!");
					return;
					
				}
				
			} else if (temp[0].equalsIgnoreCase("#builder") || temp[0].equalsIgnoreCase("#b") || temp[0].equalsIgnoreCase("#build")){
				
				if (Channels.channelContainsPlayer("builder", p.getName())){
					
					print_to = "builder";
					
				} else if (p.hasPermission(PermManager.getPerm("chan-builder"))){
					
					DoCheckForPerms(p);
					print_to = "builder";
					
				} else {
					
					e.setCancelled(true);
					p.sendMessage(Vars.Ntrl + "Oops. You don't have permission to speak in that channel!");
					return;
					
				}
				
			} else if (temp[0].equalsIgnoreCase("#global") || temp[0].equalsIgnoreCase("#g")){
				
				if (Channels.channelContainsPlayer("global", p.getName())){
					
					print_to = "global";
					
				} else if (p.hasPermission(PermManager.getPerm("chan-global"))){
					
					DoCheckForPerms(p);
					print_to = "global";
					
				} else {
					
					e.setCancelled(true);
					p.sendMessage(Vars.Ntrl + "Oops. You don't have permission to speak in that channel!");
					return;
					
				}
				
			} else {
				
				p.sendMessage(Vars.Ntrl + "Oops. Channel '" + Vars.Neg + temp[0].replaceAll("#", "") + Vars.Ntrl + "' doesn't exist!");
				e.setCancelled(true);
				return;
				
			}
			
		} else {
			
			print_to = Channels.getActiveChan(p.getName());
			
		}
				
		if (print_to == null || print_to.equals("public")){
			
			msg = Channels.PUBLIC_MCLR + msg;
			String premsg = null;
			if (Vars.use_ranks && Vars.getPlayersRank(p) != null){
				
				premsg = ChatColor.DARK_RED + "[" + Vars.getPlayersRank(p).toChatString() + ChatColor.DARK_RED + "] <" + pname + ChatColor.DARK_RED + "> ";
				
			} else {
				premsg = Channels.PUBLIC_FORMAT.replaceAll("%player%", pname);
			}
			ArrayList<Player> tonotify = new ArrayList<Player>();
			
			int i = 0;
			boolean TooManyTags = false;
			
			ArrayList<Player> tocheck = new ArrayList<Player>();
			for (Player tempp : e.getRecipients()){
				tocheck.add(tempp);
			}
			UUID uuid = p.getUniqueId();
			
			for (Player possible : tocheck){
				
				if (Vars.getIgnoredAsUUIDS(possible.getUniqueId()).contains(uuid)){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				} else if (!Vars.chatIsOn(possible.getUniqueId())){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				}
				
				if (msg.contains(possible.getName())){
					
					if (UsefulUtils.IsVanished(possible.getName())){continue;}
					
					if (i >= 2){
						
						TooManyTags = true;
						break;
						
					} else {
						
						i = i + 1;						
						msg = msg.replaceFirst(Pattern.quote(possible.getName()), UsefulUtils.GetNameWithColorsOnly(possible, true) + Channels.PUBLIC_MCLR);
						e.getRecipients().remove(possible);
						tonotify.add(possible);
					
					}
				}
			}
						
			e.setMessage(msg);
			e.setFormat(premsg + Channels.PUBLIC_MCLR + "%2$s");
			
			if (TooManyTags){
				p.sendMessage(Vars.Ntrl + "Some player tags were excluded as 2 is the max per message.");
				
			}
			for (Player tagged : tonotify){
				tagged.sendMessage(premsg + Vars.Pos + "*" + msg + Vars.Pos + "*");
				
			}
			
		} else if (print_to.equals("vip")){
		
			msg = Channels.VIP_MCLR + msg;
			String premsg = Channels.VIP_FORMAT.replaceAll("%player%", pname);
			ArrayList<Player> tonotify = new ArrayList<Player>();
			
			int i = 0;
			boolean TooManyTags = false;
			
			ArrayList<Player> tocheck = new ArrayList<Player>();
			for (Player tempp : e.getRecipients()){
				tocheck.add(tempp);
			}
			
			UUID uuid = p.getUniqueId();
			
			for (Player possible : tocheck){
				
				if (Vars.getIgnoredAsUUIDS(possible.getUniqueId()).contains(uuid)){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				} else if (!Vars.chatIsOn(possible.getUniqueId())){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				}
				
				if (!Channels.vipContains(possible)){
					
					e.getRecipients().remove(possible);
					continue;
					
				}
				
				if (msg.contains(possible.getName())){
					
					if (UsefulUtils.IsVanished(possible.getName())){continue;}
					
					if (i >= 2){
						
						TooManyTags = true;
						break;
						
					} else {
						
						i = i + 1;						
						msg = msg.replaceFirst(Pattern.quote(possible.getName()), UsefulUtils.GetNameWithColorsOnly(possible, true) + Channels.VIP_MCLR);
						e.getRecipients().remove(possible);
						tonotify.add(possible);
					
					}
				}
			}
			
			e.setMessage(msg);
			e.setFormat(premsg + Channels.VIP_MCLR + "%2$s");
			
			if (TooManyTags){
				
				p.sendMessage(Vars.Ntrl + "Some player tags were excluded as 2 is the max per message.");
			}
			for (Player tagged : tonotify){
				
				tagged.sendMessage(premsg + Vars.Pos + "*" + msg + Vars.Pos + "*");
			}
			
		} else if (print_to.equals("staff")){
			
			msg = Channels.STAFF_MCLR + msg;
			String premsg = Channels.STAFF_FORMAT.replaceAll("%player%", pname);
			ArrayList<Player> tonotify = new ArrayList<Player>();
			
			int i = 0;
			boolean TooManyTags = false;
			
			ArrayList<Player> tocheck = new ArrayList<Player>();
			for (Player tempp : e.getRecipients()){
				tocheck.add(tempp);
			}
			
			UUID uuid = p.getUniqueId();
			
			for (Player possible : tocheck){
				
				if (Vars.getIgnoredAsUUIDS(possible.getUniqueId()).contains(uuid)){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				} else if (!Vars.chatIsOn(possible.getUniqueId())){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				}
				
				if (!Channels.staffContains(possible)){
					
					e.getRecipients().remove(possible);
					continue;
					
				}
				
				if (msg.contains(possible.getName())){
					
					if (UsefulUtils.IsVanished(possible.getName())){continue;}
					
					if (i >= 2){
						
						TooManyTags = true;
						break;
						
					} else {
						
						i = i + 1;						
						msg = msg.replaceFirst(Pattern.quote(possible.getName()), UsefulUtils.GetNameWithColorsOnly(possible, true) + Channels.STAFF_MCLR);
						e.getRecipients().remove(possible);
						tonotify.add(possible);
					
					}
				}
			}
			
			e.setMessage(msg);
			e.setFormat(premsg + Channels.STAFF_MCLR + "%2$s");
			
			if (TooManyTags){
				p.sendMessage(Vars.Ntrl + "Some player tags were excluded as 2 is the max per message.");
				
			}
			for (Player tagged : tonotify){
				tagged.sendMessage(premsg + Vars.Pos + "*" + msg + Vars.Pos + "*");
				
			}
			
		} else if (print_to.equals("builder")){
			
			msg = Channels.BUILDER_MCLR + msg;
			String premsg = Channels.BUILDER_FORMAT.replaceAll("%player%", pname);
			ArrayList<Player> tonotify = new ArrayList<Player>();
			
			int i = 0;
			boolean TooManyTags = false;
			
			ArrayList<Player> tocheck = new ArrayList<Player>();
			for (Player tempp : e.getRecipients()){
				tocheck.add(tempp);
			}
			
			UUID uuid = p.getUniqueId();
			
			for (Player possible : tocheck){
				
				if (Vars.getIgnoredAsUUIDS(possible.getUniqueId()).contains(uuid)){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				} else if (!Vars.chatIsOn(possible.getUniqueId())){
					
					if (!possible.getName().equals(p.getName())){
						e.getRecipients().remove(possible);
						continue;
					}
					
				}
				
				if (!Channels.builderContains(possible)){
					
					e.getRecipients().remove(possible);
					continue;
					
				}
				
				if (msg.contains(possible.getName())){
					
					if (UsefulUtils.IsVanished(possible.getName())){continue;}
					
					if (i >= 2){
						
						TooManyTags = true;
						break;
						
					} else {
						
						i = i + 1;						
						msg = msg.replaceFirst(Pattern.quote(possible.getName()), UsefulUtils.GetNameWithColorsOnly(possible, true) + Channels.BUILDER_MCLR);
						e.getRecipients().remove(possible);
						tonotify.add(possible);
					
					}
				}
			}
			
			e.setMessage(msg);
			e.setFormat(premsg + Channels.BUILDER_MCLR + "%2$s");
			
			if (TooManyTags){
				p.sendMessage(Vars.Ntrl + "Some player tags were excluded as 2 is the max per message.");
				
			}
			for (Player tagged : tonotify){
				tagged.sendMessage(premsg + Vars.Pos + "*" + msg + Vars.Pos + "*");
				
			}
			
		} else if (print_to.equals("global")){
			
			msg = Channels.GLOBAL_MCLR + msg;
			String premsg = Channels.GLOBAL_FORMAT.replaceAll("%player%", pname);
			ArrayList<Player> tonotify = new ArrayList<Player>();
			
			int i = 0;
			boolean TooManyTags = false;
			
			ArrayList<Player> tocheck = new ArrayList<Player>();
			for (Player tempp : e.getRecipients()){
				tocheck.add(tempp);
			}
			
			for (Player possible : tocheck){
				
				if (msg.contains(possible.getName())){
					
					if (UsefulUtils.IsVanished(possible.getName())){continue;}
					
					if (i >= 2){
						
						TooManyTags = true;
						break;
						
					} else {
						
						i = i + 1;						
						msg = msg.replaceFirst(Pattern.quote(possible.getName()), UsefulUtils.GetNameWithColorsOnly(possible, true) + Channels.GLOBAL_MCLR);
						e.getRecipients().remove(possible);
						tonotify.add(possible);
					
					}
				}
			}
			
			e.setMessage(msg);
			e.setFormat(premsg + Channels.GLOBAL_MCLR + "%2$s");
			
			if (TooManyTags){
				p.sendMessage(Vars.Ntrl + "Some player tags were excluded as 2 is the max per message.");
				
			}
			for (Player tagged : tonotify){
				tagged.sendMessage(premsg + Vars.Pos + "*" + msg + Vars.Pos + "*");
				
			}
			
		} else {
			p.sendMessage(ChatColor.RED + "Error has occurred. Tell an admin as soon as possible.");
			
		}
		return;
		
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		
		ArrayList<String> chans = DoCheckForPerms(p);
		
		if (!chans.isEmpty()){
			
			StringBuilder sb = new StringBuilder();
			String comma = w + "";
			for (String s : chans){
				sb.append(comma).append(s);
				comma = g + ", " + w;
				
			}
			
			p.sendMessage(b + "==================================================");
			p.sendMessage(g + "You are in these chat channels: " + sb.toString().trim());
			p.sendMessage(g + "You are currently speaking in the " + w + "public" + g + " channel.");
			p.sendMessage(g + "For help on channel chatting, type: " + w + "/chan help");
			p.sendMessage(b + "==================================================");
			

		}
		
		
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e){
		Channels.completelyRemovePlayer(e.getPlayer().getName(), true);
		
	}
	
	public static ArrayList<String> DoCheckForPerms(Player p){
		
		ArrayList<String> chans = new ArrayList<String>();
		if (p.hasPermission(PermManager.getPerm("chan-staff"))){
			Channels.addToStaffChannel(p.getName());
			chans.add("staff");
			
		}
		if (p.hasPermission(PermManager.getPerm("chan-vip"))){
			Channels.addToVipChannel(p.getName());
			chans.add("vip");
			
		}
		if (p.hasPermission(PermManager.getPerm("chan-global"))){
			Channels.addToGlobalChannel(p.getName());
			chans.add("global");
			
		}
		if (p.hasPermission(PermManager.getPerm("chan-builder"))){
			Channels.addToBuilderChannel(p.getName());
			chans.add("builder");
			
		}
		
		return chans;
		
	}
	
}
