package org.jkjkkj.TypicalChat.ChatChannels;

import org.bukkit.ChatColor;

public enum Rank {

	RECOGNIZED(ChatColor.DARK_AQUA + "Recognized"), TCARCHITECT(ChatColor.DARK_AQUA + "" + ChatColor.ITALIC + "TCArchitect"), FLORIST(ChatColor.LIGHT_PURPLE + "Florist"), PIXART(ChatColor.DARK_PURPLE + "PixArt"), SCULPTOR(ChatColor.DARK_BLUE + "Sculptor"), DESIGNER(ChatColor.BLUE + "Designer"), PIONEER(ChatColor.BLACK + "Pioneer"), INNOVATOR(ChatColor.GREEN + "Innovator"), SKYART(ChatColor.AQUA + "SkyArt");
	
	private String value;
	
	private Rank(String val){
		this.value = val;
	}
	
	public String toChatString(){		
		
		return this.value;
		
	}
}
