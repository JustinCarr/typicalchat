package org.jkjkkj.TypicalChat.ChatChannels;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.TypicalChat.TypicalChat;
import org.jkjkkj.TypicalChat.Vars;

public class Channels {
	
	public static int NAME_TYPE = 1;
	public static String PUBLIC_FORMAT = "";
	public static String STAFF_FORMAT = "";
	public static String VIP_FORMAT = "";
	public static String BUILDER_FORMAT = "";
	public static String GLOBAL_FORMAT = "";
	
	public static String PUBLIC_MCLR = "";
	public static String STAFF_MCLR = "";
	public static String VIP_MCLR = "";
	public static String BUILDER_MCLR = "";
	public static String GLOBAL_MCLR = "";
	
	private static HashMap<String, String> player_chans = new HashMap<String, String>();
	
	
	private static ArrayList<String> staff = new ArrayList<String>();
	private static ArrayList<String> vip = new ArrayList<String>();
	private static ArrayList<String> builder = new ArrayList<String>();
	private static ArrayList<String> global = new ArrayList<String>();
	
	public Channels(TypicalChat plugin){
		
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new BukkitRunnable(){
			
			public void run(){
				
				for (Player p : Bukkit.getOnlinePlayers()){
					
					doCheck(p);
					
				}
				
			}
			
		}, 40L, 180L);
		
	}
	
	public static void doCheck(Player p){
		
		String n = p.getName();
		completelyRemovePlayer(n, false);
		
		if (p.hasPermission("chat.channel.global")){
			global.add(n);
		}
		if (p.hasPermission("chat.channel.vip")){
			vip.add(n);
		}
		if (p.hasPermission("chat.channel.staff")){
			staff.add(n);
		}
		if (p.hasPermission("chat.channel.builder")){
			builder.add(n);
		}
		if (player_chans.containsKey(n)){
			if (!channelContainsPlayer(player_chans.get(n), n)){
				player_chans.remove(n);
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are no longer allowed to chat in channel '" + Vars.Neg +  player_chans.get(n) + Vars.Ntrl + "'");
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "public" + Vars.Ntrl + " channel!");
			}
		}
		
	}
	
	public static void completelyRemovePlayer(String p, boolean ActiveChanToo){
		
		if (ActiveChanToo){
			player_chans.remove(p);
		}
		staff.remove(p);
		vip.remove(p);
		builder.remove(p);
		global.remove(p);
		return;
		
	}
	
	public static void addToStaffChannel(String p){
		
		if (!staff.contains(p)){
			staff.add(p);

		}
		
	}
	
	public static void addToVipChannel(String p){
		
		if (!vip.contains(p)){
			vip.add(p);

		}
		
	}
	
	public static void addToBuilderChannel(String p){
		
		if (!builder.contains(p)){
			builder.add(p);

		}		
		
	}
	
	public static void addToGlobalChannel(String p){
		
		if (!global.contains(p)){
			global.add(p);

		}
		
	}
	
	public static String getActiveChan(String p){
		String players_chan = null;
		if (player_chans.containsKey(p)){
			players_chan = player_chans.get(p);
		} else {
			players_chan = "public";
		}
		return players_chan;
		
	}
	
	public static boolean channelContainsPlayer(String channel, String p){
		
		if (channel.equalsIgnoreCase("public")){
			return true;
			
		} else if (channel.equalsIgnoreCase("builder")){
			if (builder.contains(p)){return true;} else {return false;}
			
		} else if (channel.equalsIgnoreCase("vip")){
			if (vip.contains(p)){return true;} else {return false;}
			
		} else if (channel.equalsIgnoreCase("staff")){
			if (staff.contains(p)){return true;} else {return false;}
			
		} else if (channel.equalsIgnoreCase("global")){
			if (global.contains(p)){return true;} else {return false;}

		} else {
			
			return false;
		}
		
	}
	
	public static boolean builderContains(Player p){
		if (builder.contains(p.getName())){
			return true;
		} else {return false;}	
	}
	public static boolean staffContains(Player p){
		if (staff.contains(p.getName())){
			return true;
		} else {return false;}	
	}
	public static boolean vipContains(Player p){
		if (vip.contains(p.getName())){
			return true;
		} else {return false;}	
	}
	public static boolean globalContains(Player p){
		if (global.contains(p.getName())){
			return true;
		} else {return false;}	
	}
	
	public static void setActiveChan(String p, String channel){
		
		if (player_chans.containsKey(p)){
			player_chans.remove(p);
		}
		if (channel.equals("public")){
			
			player_chans.put(p, "public");
			
		} else if (channel.equals("staff")){
			
			player_chans.put(p, "staff");
			
		} else if (channel.equals("vip")){
			
			player_chans.put(p, "vip");
			
		} else if (channel.equals("builder")){
			
			player_chans.put(p, "builder");
			
		} else if (channel.equals("global")){
			
			player_chans.put(p, "global");
			
		} else {
			
			TypicalChat.LogMessage("Error occured in setting " + p + "'s active channel!", true);
			
		}
		
	}
	
	
	
	
}
