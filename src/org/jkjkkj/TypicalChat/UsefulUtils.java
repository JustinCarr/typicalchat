package org.jkjkkj.TypicalChat;

import java.util.ArrayList;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;

public class UsefulUtils {

	private static Chat chat = null;
	
	public static boolean IsTypical(Player p){
		
		if (setupChat()){
			
			if (chat.getPrimaryGroup(p).equalsIgnoreCase("Typical")){
				return true;
			}
			
		}
		return false;
		
	}

	public static String GetNameWithColorsOnly(Player p, boolean ConsiderNicknames){
		
		String pname = "";
		
		if (setupChat()){
						
			String actualname = "";
			
			if (ConsiderNicknames){
				
				if (TypicalChat.isEssentialsEnabled() && TypicalChat.isEssentialsThere() && 
						!p.hasPermission(PermManager.getPerm("nonicknamesallowed"))){

					final Essentials essentials = (Essentials) Bukkit.getServer().getPluginManager().getPlugin("Essentials");
										
					actualname = essentials.getOfflineUser(p.getName()).getDisplayName();
					String pinitial = ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(p));
					pname = ChatColor.getLastColors(pinitial) + actualname;
					return pname; 
					
				} else {
					actualname = p.getName();
				}
				
			} else {
				actualname = p.getName();
			}
			
			String pinitial = ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(p));
			pname = ChatColor.getLastColors(pinitial) + actualname;
			
		} else {
			pname = p.getName();
			
		}
		return pname;
		
	}
	
	public static boolean IsVanished(String p){
		
		if (TypicalChat.isEssentialsThere() && TypicalChat.isEssentialsEnabled()){
			
			final Essentials essentials = (Essentials) Bukkit.getServer().getPluginManager().getPlugin("Essentials");
		
			User u = essentials.getOfflineUser(p);
			
			if (u != null){
				if (u.isVanished()){
					return true;
				}
			
			}
			
		}
		
		return false;
		
	}
	
	public static String GetNameWithPrefix(Player p, boolean ConsiderNicknames){
		
		String pname = "";
		
		if(setupChat()){
			
			String actualname = "";

			if (ConsiderNicknames){

				if (TypicalChat.isEssentialsEnabled() && TypicalChat.isEssentialsThere() && 
						!p.hasPermission(PermManager.getPerm("nonicknamesallowed"))){
		
					final Essentials essentials = (Essentials) Bukkit.getServer().getPluginManager().getPlugin("Essentials");
					
					actualname = essentials.getOfflineUser(p.getName()).getDisplayName();
					
				} else {
					actualname = p.getName();
				}
				
			} else {
				actualname = p.getName();
			}
			pname = ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(p)) + actualname;
		} else {
			pname = p.getName();
		}
		
		return pname;
	}
	
    private static boolean setupChat()
    {
        RegisteredServiceProvider<Chat> chatProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }
        
        return (chat != null);
    }
    
    public static boolean isMuted(String p){
    	
		if (TypicalChat.isEssentialsEnabled() && TypicalChat.isEssentialsThere()){
			
			final Essentials essentials = (Essentials) Bukkit.getServer().getPluginManager().getPlugin("Essentials");
			User user = essentials.getOfflineUser(p);
			
			if (user != null){
				
				if (user.isMuted()){
					return true;
				}
								
			}
			
		}
			
		return false;	
    	
    }
    
    public static String GetWordsNotAllowed(Player p, String input){
    	
    	String output = null;
    	
    	if (!p.hasPermission(PermManager.getPerm("canswear"))){
    		for (String s : Vars.FILTERED_WORDS){
    			if (input.contains(s)){
    				output = s;
    				break;
    				
    			}
    		}
    	}
    	
    	return output;
    	
    }
	
	public static ArrayList<Player> getStaff(){
		
		ArrayList<Player> players = new ArrayList<Player>();
		
		if (TypicalChat.isEssentialsThere() && TypicalChat.isEssentialsEnabled()){
			for (Player p : Bukkit.getServer().getOnlinePlayers()){
				
				if (p.hasPermission(PermManager.getPerm("who-staff")) || p.isOp()){
					
					if (p.hasPermission(PermManager.getPerm("who-owner"))){
						
						if (!IsVanished(p.getName())){
							players.add(p);
						}
						
					} else {
						players.add(p);
					}
				}
			}
		} else {
			for (Player p : Bukkit.getServer().getOnlinePlayers()){
				
				if (p.hasPermission(PermManager.getPerm("who-staff")) || p.isOp()){
					
					players.add(p);
				}
			}
		}
		
		
		
		return players;
		
	}
    
	
}
