package org.jkjkkj.TypicalChat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.jkjkkj.TypicalChat.ChatChannels.Channels;
import org.jkjkkj.TypicalChat.ChatChannels.Rank;

public class Vars {

	private static TypicalChat plugin;
	private static File CFile;
	private static FileConfiguration Config;
	private static File PFile;
	private static FileConfiguration PConfig;
	private static File TCXFile;
	private static FileConfiguration TCXConfig;
	
	public static ArrayList<String> BROADCAST_MESSAGE = new ArrayList<String>();
	public static ArrayList<String> WHO_MESSAGE = new ArrayList<String>();
	public static Integer BROADCAST_INTERVAL = 10;
	public static String WHO_COMMA_COLOR = ChatColor.translateAlternateColorCodes('&', "&6");
	public static String STAFF = ChatColor.GRAY + "Unkown message.";
	public static String VOTES = ChatColor.GRAY + "Unkown message.";
	public static String TC = ChatColor.DARK_PURPLE + "Everyone loves TypicalCraft!";
	
	public static final String CHAT_BAR_LINE = "==================================================";
	public static final String NO_TALKING = ChatColor.RED + "Chat is currently disabled, sorry!";
	public static final String NO_PERM_MSG = ChatColor.RED + "You don't have permission for this!";
	public static final String NOT_ONLINE = ChatColor.RED + "Oops. Player '%player%' was not found!";
	public static final String NOT_IN_CONSOLE = ChatColor.RED + "Oops. This cannot be done in the console!";
	public static final String NO_SEND_MUTED = ChatColor.RED + "Oops. You cannot send messages because you're muted!";
	public static final String NO_EMOTE_MUTED = ChatColor.RED + "Oops. No broadcasting emotions while you're muted!";
	public static final String NO_CHAT_MUTED = ChatColor.RED + "Oops. You cannot chat because you're muted!";
	public static final String Pos = ChatColor.GREEN + "" + ChatColor.BOLD;
	public static final String Neg = ChatColor.DARK_RED + "" + ChatColor.BOLD;
	public static final String Ntrl = ChatColor.RED + "";
	public static final String NO_TALKING_SWORE = ChatColor.RED + "Oops. You were muted for " + Neg + "10" + Ntrl + " seconds for using a bad word!";;
	public static final String NOTICE_PREFIX = Neg + "[" + Pos + "##" + Neg + "] " + Ntrl;
	
	public static boolean CHAT_ENABLED = true;
	public static String JOIN_MSG = "";
	public static String LEAVE_MSG = "";
	public static String ME_FORMAT = "";
	public static String ME_COLOR = "";
	public static int ME_NAME_TYPE = 1;
	public static int NAME_TYPE_JOINLEAVE = 1;
	public static int NAME_TYPE_MESSAGING = 1;
	public static String INCOMING_MSG = "";
	public static String OUTGOING_MSG = "";
	public static String SPYING_MSG = "";
	public static int IGNORE_LIMIT = -1;
	public static List<String> FILTERED_WORDS = new ArrayList<String>();
	public static ArrayList<String> USE_SWEARS = new ArrayList<String>();
	
	private static Map<UUID, List<UUID>> ignored = new HashMap<UUID, List<UUID>>();
	private static Map<UUID, String> chat_off = new HashMap<UUID, String>();
	private static Map<UUID, String> last_reply = new HashMap<UUID, String>();
	private static ArrayList<UUID> socialspy_disabled = new ArrayList<UUID>();
	
	//Below is for use by PlotMod and TCCreativeUtils
	public static boolean use_ranks = false;
	private static HashMap<UUID, Rank> pranks = new HashMap<UUID, Rank>();
	public static String regular_plotworld = "";
	public static String donor_plotworld = "";
	public static boolean COMP_LOCKED = false;
	
	public Vars(TypicalChat chat){
		plugin = chat;
		

		InitializeFile(true);
		ReloadConfig(true);
		InitializeFile(false);
		ReloadConfig(false);
		InitializeTCXFile();
		ReloadTCXConfig();
		GetVars(true);
		GetVars(false);
		GetVarsFromTCXConfig();
		
	}
	
	public static void Nullify(){
		
		ignored.clear();
		last_reply.clear();
		chat_off.clear();
		socialspy_disabled.clear();
		FILTERED_WORDS.clear();
		USE_SWEARS.clear();
		BROADCAST_MESSAGE.clear();
		WHO_MESSAGE.clear();
		pranks.clear();
		COMP_LOCKED = false;
		
	}
	
	public static void ReloadFiles(boolean isPlayer, boolean alsoTCExtras){
		
		if (alsoTCExtras){
			
			InitializeTCXFile();
			ReloadTCXConfig();
			
		} else if (isPlayer){
			InitializeFile(true);
			ReloadConfig(true);
		} else {
			InitializeFile(false);
			ReloadConfig(false);
		}
	
	}
	
	public static Rank getPlayersRank(Player p){
		
		if (pranks.containsKey(p.getUniqueId())){
			return pranks.get(p.getUniqueId());
		} else {
			return null;
		}
		
	}
	
	public static void removePlayersRank(UUID u){
		pranks.remove(u);
	}
	
	public static void setPlayersRank(UUID u, Rank rank){
		
		pranks.put(u, rank);
		
	}
	
	private static void ReloadConfig(boolean isPlayers){
		
		if (isPlayers){
			
			if (PFile==null){
				PFile = new File(plugin.getDataFolder(), "player_data.yml");	
			}
			PConfig = YamlConfiguration.loadConfiguration(PFile);
			InputStream stream = plugin.getResource("player_data.yml");
			if (stream != null){
				YamlConfiguration thestream = YamlConfiguration.loadConfiguration(stream);
				PConfig.setDefaults(thestream);
			}
			
		} else {
		
			if (CFile==null){
				CFile = new File(plugin.getDataFolder(), "config.yml");	
			}
			Config = YamlConfiguration.loadConfiguration(CFile);
			InputStream stream = plugin.getResource("config.yml");
			if (stream != null){
				YamlConfiguration thestream = YamlConfiguration.loadConfiguration(stream);
				Config.setDefaults(thestream);
			}
			
		}
	}
	private static void ReloadTCXConfig(){
		if (TCXFile==null){
			TCXFile = new File(plugin.getDataFolder(), "tcx_config.yml");	
		}
		TCXConfig = YamlConfiguration.loadConfiguration(TCXFile);
		InputStream stream = plugin.getResource("tcx_config.yml");
		if (stream != null){
			YamlConfiguration thestream = YamlConfiguration.loadConfiguration(stream);
			TCXConfig.setDefaults(thestream);
		}
	}

	private static void InitializeFile(boolean isPlayers){
		
		if (isPlayers){
			
			if (PFile == null){
				PFile = new File(plugin.getDataFolder(), "player_data.yml");
			}
			if (!PFile.exists()){
				plugin.saveResource("player_data.yml", false);
			}
		
		} else {
		
			if (CFile == null){
				CFile = new File(plugin.getDataFolder(), "config.yml");
			}
			if (!CFile.exists()){
				plugin.saveResource("config.yml", false);
			}
		
		}
	}
	private static void InitializeTCXFile(){
		if (TCXFile == null){
			TCXFile = new File(plugin.getDataFolder(), "tcx_config.yml");
		}
		if (!TCXFile.exists()){
			plugin.saveResource("tcx_config.yml", false);
		}
	}

	private static void SaveConfig(boolean isPlayers){
		
		if (isPlayers){
			
			if (PFile == null || PConfig == null){
				TypicalChat.LogMessage("Saving has been haulted. File not found.", true);
				return;
			}
			try {
				PConfig.save(PFile);
			} catch (IOException e){
				TypicalChat.LogMessage("Something happened while saving the player data...", true);
			} catch (IllegalArgumentException e){
				TypicalChat.LogMessage("Something happened while saving the player data...", true);
			}
			
		} else {
			
			if (CFile == null || Config == null){
				TypicalChat.LogMessage("Saving has been haulted. File not found.", true);
				return;
			}
			try {
				Config.save(CFile);
			} catch (IOException e){
				TypicalChat.LogMessage("Something happened while saving the player data...", true);
			} catch (IllegalArgumentException e){
				TypicalChat.LogMessage("Something happened while saving the player data...", true);
			}
			
		}
		
	}

	/*private static void SaveTCXConfig(){
		if (TCXFile == null || TCXConfig == null){
			TypicalChat.LogMessage("Saving has been haulted. File not found.", true);
			return;
		}
		try {
			TCXConfig.save(TCXFile);
		} catch (IOException e){
			TypicalChat.LogMessage("[TCExtras] Something happened while saving the config...", true);
		} catch (IllegalArgumentException e){
			TypicalChat.LogMessage("[TCExtras] Something happened while saving the config...", true);
		}
	}*/
	
	public static void GetVars(boolean isPlayers){
		
		if (isPlayers){
			
			if (PConfig.isConfigurationSection("players")){
				
				for (String n : PConfig.getConfigurationSection("players").getKeys(false)){
					
					UUID uuid_key = null;
					try {
						uuid_key = UUID.fromString(n);
					} catch (Exception e){
						PConfig.set("players." + n, null);
						break;
					}
					
					ArrayList<UUID> temp = new ArrayList<UUID>();
					
					for (String s : PConfig.getStringList("players." + n)){
						
						try {
							temp.add(UUID.fromString(s));
						} catch (Exception e){
							temp.clear();
							break;
						}
						
					}
					
					if (temp.isEmpty()){
						PConfig.set("players." + n, null);
					} else {
						ignored.put(uuid_key, temp);
					}
					
				}
				SaveConfig(true);
				
			} else {
				
				PConfig.set("players", null);
				SaveConfig(true);
				
			}
			
			if (PConfig.isConfigurationSection("chat_off")){
				
				boolean toset = false;
				for (String n : PConfig.getConfigurationSection("chat_off").getKeys(false)){
					
					UUID uuid = null;
					try {
						
						uuid = UUID.fromString(n);
																		
					} catch (IllegalArgumentException e){
						
						toset = true;
						continue;
						
					}
					String s = PConfig.getString("chat_off." + n);
					if (s != null){
						chat_off.put(uuid, s);
					} else {
						toset = true;
					}
					
				}
				
				if (toset) {
					
					for (Entry<UUID, String> en : chat_off.entrySet()){
						PConfig.set("chat_off" + en.getKey().toString(), en.getValue());
					}
					
					SaveConfig(true);
					
				}
				
			}
			
			int lim = PConfig.getInt("max_ignored_per_player");
			if (lim > 0){
				IGNORE_LIMIT = lim;
			} else {
				IGNORE_LIMIT = -1;
			}
			
		} else {
		
			CHAT_ENABLED = Config.getBoolean("chat.enabled");
			JOIN_MSG = ChatColor.translateAlternateColorCodes('&', Config.getString("messages.joinleave.join"));
			LEAVE_MSG = ChatColor.translateAlternateColorCodes('&', Config.getString("messages.joinleave.quit"));
			
			String s = Config.getString("messages.joinleave.name_type").toLowerCase();
			
			if (s.equals("color") || s.equals("colors") || s.equals("colored")){
				
				NAME_TYPE_JOINLEAVE = 2;
			
			} else if (s.equals("prefix") || s.equals("prefixes") || s.equals("prefixed")){
				
				NAME_TYPE_JOINLEAVE = 3;
			
			} else {
				
				NAME_TYPE_JOINLEAVE = 1;
				
			}
			
			String s2 = Config.getString("chat.private_messaging.name_type");
			
			if (s2.equals("color") || s2.equals("colors") || s2.equals("colored")){
				
				NAME_TYPE_MESSAGING = 2;
			
			} else if (s2.equals("prefix") || s2.equals("prefixes") || s2.equals("prefixed")){
				
				NAME_TYPE_MESSAGING = 3;
			
			} else {
				
				NAME_TYPE_MESSAGING = 1;
				
			}
			int it = Config.getInt("chat.emote.message_color");
			ME_FORMAT = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.emote.format"));
			ME_COLOR = ChatColor.translateAlternateColorCodes('&', String.valueOf("&" + it));
			String me_type = Config.getString("chat.emote.name_type");
			if (me_type.equals("color") || me_type.equals("colors") || me_type.equals("colored")){
				ME_NAME_TYPE = 2;
			
			} else if (me_type.equals("prefix") || me_type.equals("prefixes") || me_type.equals("prefixed")){
				ME_NAME_TYPE = 3;
			
			} else {
				ME_NAME_TYPE = 1;
				
			}
			
			FILTERED_WORDS = Config.getStringList("word_blacklist");
			INCOMING_MSG = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.private_messaging.incoming_msg"));
			OUTGOING_MSG = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.private_messaging.outgoing_msg"));
			SPYING_MSG = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.private_messaging.social_spy"));
			
			Channels.PUBLIC_MCLR = ChatColor.translateAlternateColorCodes('&', "&" + Config.getString("chat.channels.public.message_color"));
			Channels.VIP_MCLR = ChatColor.translateAlternateColorCodes('&', "&" + Config.getString("chat.channels.vip.message_color"));
			Channels.STAFF_MCLR = ChatColor.translateAlternateColorCodes('&', "&" + Config.getString("chat.channels.staff.message_color"));
			Channels.BUILDER_MCLR = ChatColor.translateAlternateColorCodes('&', "&" + Config.getString("chat.channels.builder.message_color"));
			Channels.GLOBAL_MCLR = ChatColor.translateAlternateColorCodes('&', "&" + Config.getString("chat.channels.global.message_color"));
			
			Channels.PUBLIC_FORMAT = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.channels.public.format"));
			Channels.VIP_FORMAT = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.channels.vip.format"));
			Channels.STAFF_FORMAT = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.channels.staff.format"));
			Channels.BUILDER_FORMAT = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.channels.builder.format"));
			Channels.GLOBAL_FORMAT = ChatColor.translateAlternateColorCodes('&', Config.getString("chat.channels.global.format"));
			
			String type = Config.getString("chat.channels.name_type");
			if (type.equals("color") || type.equals("colors") || type.equals("colored")){
				Channels.NAME_TYPE = 2;
			
			} else if (type.equals("prefix") || type.equals("prefixes") || type.equals("prefixed")){
				Channels.NAME_TYPE = 3;
			
			} else {
				Channels.NAME_TYPE = 1;
				
			}
		
		}
	
	}
	
	public static void removeFromReplyList(UUID uuid){
		
		last_reply.remove(uuid);
		
	}
	
	public static void setToReplyTo(UUID uuid, String uuid_lastsender, boolean IsServer){
		
		if (IsServer){
			last_reply.put(uuid, "Server");
		} else {
			
			try{
				
				UUID.fromString(uuid_lastsender);
				last_reply.put(uuid, uuid_lastsender);
				
			} catch (IllegalArgumentException e){
				
				last_reply.put(uuid, "Server");
				
			}
			
		}
		
	}
	
	public static String getToReplyTo(UUID uuid){
		
		return last_reply.get(uuid);
		
	}
	
	public static ArrayList<String> getAllWithChatOff(){
		
		ArrayList<String> temp = new ArrayList<String>();
		for (Entry<UUID, String> s : chat_off.entrySet()){
			temp.add(s.getValue());
		}
		
		return temp;
		
	}
	
	public static void updateChatOnJoin(UUID pname, String actual_name){
		
		if (chat_off.containsKey(pname)){
			
			if (!chat_off.get(pname).equals(actual_name)){
				
				chat_off.remove(pname);
				chat_off.put(pname, actual_name);
				
				PConfig.set("chat_off." + pname.toString(), null);
				PConfig.set("chat_off" + pname.toString(), String.valueOf(actual_name));
				SaveConfig(true);
				
			}
			
		}
		
	}
	
	public static boolean chatIsOn(UUID pname) throws NullPointerException {
		
		if (chat_off.containsKey(pname)){
			
			return false;
		} else {
			return true;
		}
		
	}
	
	public static void setChat(UUID pname, String pname_actual, boolean on) throws NullPointerException {
				
		if (!PConfig.isConfigurationSection("chat_off")){
			
			PConfig.set("chat_off", null);
			SaveConfig(true);
			chat_off.clear();
			
		}
		
		if (on){
			
			String uuid_string = pname.toString();
			if (PConfig.isString("chat_off." + uuid_string)){
				PConfig.set("chat_off." + uuid_string, null);
				SaveConfig(true);
			}
			if (chat_off.containsKey(pname)){
				chat_off.remove(pname);
			}
			
		} else {
						
			String uuid_string = pname.toString();
			if (!PConfig.isString("chat_off." + uuid_string)){
				PConfig.set("chat_off." + uuid_string, pname_actual);
				SaveConfig(true);
			}
			if (!chat_off.containsKey(pname)){
				chat_off.put(pname, pname_actual);
			}
			
		}
				
	}
	
	public static ArrayList<String> getIgnoredAsStrings(UUID puuid){
		
		ArrayList<String> type = new ArrayList<String>();
		
		if (ignored.containsKey(puuid)){
			
			try {
				
				Collection<String> col = UUIDManager.getNamesFromUUID(ignored.get(puuid)).values();
				type.addAll(col);
				
			} catch (Exception e){
				
				ignored.remove(puuid);
				PConfig.set("players." + puuid.toString(), null);
				SaveConfig(true);
				
			}
						
		}
		
		return type;
				
	}
	
	public static List<UUID> getIgnoredAsUUIDS(UUID puuid){
		
		List<UUID> type = new ArrayList<UUID>();
		
		if (ignored.containsKey(puuid)){
			
			type = ignored.get(puuid);
						
		}
		
		return type;
				
	}
	
	public static void setIgnored(UUID pplayer, UUID ttoignore){
		
		String player = pplayer.toString();
		String toignore = ttoignore.toString();
		
		if (PConfig.isList("players." + player)){
			
			List<String> curr_ignored = PConfig.getStringList("players." + player);
			if (!curr_ignored.contains(toignore)){
				
				curr_ignored.add(toignore);
				PConfig.set("players." + player, curr_ignored);				
				SaveConfig(true);
				
			}
			if (ignored.containsKey(pplayer)){
				
				ArrayList<UUID> toset = new ArrayList<UUID>();
				
				for (UUID u : ignored.get(pplayer)){
					
					toset.add(u);
					
				}
				if (!toset.contains(ttoignore)){
					toset.add(ttoignore);
				}
				ignored.put(pplayer, toset);
				
			} else {
				
				ignored.put(pplayer, Arrays.asList(ttoignore));
				
			}
			
		} else {
			
			PConfig.set("players." + player, Arrays.asList(toignore));
			SaveConfig(true);

			ignored.remove(pplayer);			
			ignored.put(pplayer, Arrays.asList(ttoignore));
			
		}
				
	}
	
	public static void removeIgnored(UUID pplayer, UUID ttoremove){
		
		String player = pplayer.toString();
		String toremove = ttoremove.toString();
		
		if (PConfig.isList("players." + player)){
			
			List<String> curr_ignored = PConfig.getStringList("players." + player);
			if (curr_ignored.contains(toremove)){
				
				curr_ignored.remove(toremove);
				if (curr_ignored.isEmpty()){
					PConfig.set("players." + player, null);
				} else {
					PConfig.set("players." + player, curr_ignored);				
				}
				SaveConfig(true);
				
			}
			if (ignored.containsKey(pplayer)){
				
				ArrayList<UUID> toset = new ArrayList<UUID>();
				
				for (UUID u : ignored.get(pplayer)){
					
					toset.add(u);
					
				}
				if (toset.contains(ttoremove)){
					toset.remove(ttoremove);
				}
				ignored.put(pplayer, toset);
				
			}
			
		} else {
			
			PConfig.set("players." + player, null);
			SaveConfig(true);

			ignored.remove(pplayer);			
			
		}
		
	}
	
	public static void ToggleChat(boolean on){
		
		if (on){
			Config.set("chat.enabled", Boolean.valueOf(true));
			SaveConfig(false);
			CHAT_ENABLED = Config.getBoolean("chat.enabled");
			CHAT_ENABLED = true;
		} else {
			Config.set("chat.enabled", Boolean.valueOf(false));
			SaveConfig(false);
			
			CHAT_ENABLED = Config.getBoolean("chat.enabled");
			CHAT_ENABLED = false;
		}
		
	}
	
	public static void GetVarsFromTCXConfig(){
		
		List<String> bm = TCXConfig.getStringList("broadcastmessage");
		for (String s : bm){
			BROADCAST_MESSAGE.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		
		
		List<String> wm = TCXConfig.getStringList("whomessage.message");
		for (String s : wm){
			WHO_MESSAGE.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		
		int i = TCXConfig.getInt("broadcastinterval");
		if (i != 0){BROADCAST_INTERVAL = i;}
		
		if (TCXConfig.isString("tc")){
			TC = ChatColor.translateAlternateColorCodes('&', TCXConfig.getString("tc"));
		}
		if (TCXConfig.isString("whomessage.comma_color")){
			WHO_COMMA_COLOR = ChatColor.translateAlternateColorCodes('&', TCXConfig.getString("whomessage.comma_color"));
		}
		if (TCXConfig.isString("staff")){
			STAFF = ChatColor.translateAlternateColorCodes('&', TCXConfig.getString("staff"));
		}
		if (TCXConfig.isString("votes")){
			VOTES = ChatColor.translateAlternateColorCodes('&', TCXConfig.getString("votes"));
		}
		
	}
	
	public static void modifySocialSpyDisabledList(Player p, boolean toadd){
		
		if (toadd){
			
			if (!socialspy_disabled.contains(p.getUniqueId())) socialspy_disabled.add(p.getUniqueId());
				
		} else {
			
			socialspy_disabled.remove(p.getUniqueId());
			
		}
		
	}
	
	public static boolean hasSocialSpyEnabled(Player p){
		if (!socialspy_disabled.contains(p.getUniqueId())){
			return true;
		} else {
			return false;
		}
	}
	

	
}
