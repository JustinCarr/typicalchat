package org.jkjkkj.TypicalChat;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

public class JoinLeaveHandler implements Listener {
	
	private TypicalChat local;
	private static Map<String, UUID> temp_ids = new HashMap<String, UUID>();
	private static Chat chat = null;
	
	public JoinLeaveHandler(TypicalChat inst){
		
		this.local = inst;
		Map<String, UUID> new_map = new HashMap<String, UUID>();
		
		for (Player p : this.local.getServer().getOnlinePlayers()){
			
			new_map.put(p.getName(), p.getUniqueId());
			
		}
		temp_ids.clear();
		temp_ids.putAll(new_map);
		
		setupChat();
		
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(PlayerJoinEvent e){
		
		Player p = e.getPlayer();
		
		if (!temp_ids.containsKey(p.getName())){
			
			temp_ids.put(p.getName(), p.getUniqueId());
			
		}
		
		Vars.updateChatOnJoin(p.getUniqueId(), p.getName());
		
		if (p.hasPermission(PermManager.getPerm("chatlocknotify"))){
			if (!Vars.CHAT_ENABLED){
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "NOTICE: Chat is currently " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "To re-enable it, use: " + Vars.Neg + "/togglechat on");
			}
		}
		
		if (p.hasPermission(PermManager.getPerm("joinleave"))){
			if (Vars.NAME_TYPE_JOINLEAVE == 3){
				e.setJoinMessage(Vars.JOIN_MSG.replaceAll("%player%", UsefulUtils.GetNameWithPrefix(p, true)));

			} else if (Vars.NAME_TYPE_JOINLEAVE == 2){
				e.setJoinMessage(Vars.JOIN_MSG.replaceAll("%player%", UsefulUtils.GetNameWithColorsOnly(p, true)));
				
			} else {
				e.setJoinMessage(Vars.JOIN_MSG.replaceAll("%player%", e.getPlayer().getName()));
			}
		} else {
			e.setJoinMessage(null);
		}
		
		final Player playr = p;
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(this.local, new Runnable(){
			
			public void run(){
				
				if (playr == null || !playr.isOnline()){return;}
				
				String name = "";
						
				if (UsefulUtils.IsTypical(playr)){
					
					name = playr.getName();
					
				} else {
					name = UsefulUtils.GetNameWithColorsOnly(playr, true);
				
				}
				
				if (name.length() > 16){
					
					name = name.substring(0, 15);
					String stemp = "";
					
					for (int i = 2; i <= 16 ;i++){
						
						try {
							playr.setPlayerListName(name);
							break;
						} catch (IllegalArgumentException ex){
							
							stemp = stemp + "*";					
							name = (name.substring(0, name.length() - i)) + stemp;
							
						}
						
						
					}
					
				} else {
					
					String stemp = "";
					boolean addedspaces = false;
					
					for (int i = 2; i <= 16 ;i++){
						
						try {
							playr.setPlayerListName(name);
							break;
						} catch (IllegalArgumentException ex){
							
							if (!addedspaces){
								
								StringBuilder sb = new StringBuilder();
								for (int newi = 1 ; newi < (16 - name.length()); newi++){
									sb.append(" ");
								}
								name = name + sb.toString();
								
							}
							
							stemp = stemp + "*";					
							name = (name.substring(0, name.length() - i)) + stemp;
							
						}
						
						
					}
					
				} 
				
			}
			
		}, 20L);
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onQuit(PlayerQuitEvent e){
		
		if (e.getPlayer().hasPermission(PermManager.getPerm("joinleave"))){
			if (Vars.NAME_TYPE_JOINLEAVE == 3){
				e.setQuitMessage(Vars.LEAVE_MSG.replaceAll("%player%", UsefulUtils.GetNameWithPrefix(e.getPlayer(), true)));

			} else if (Vars.NAME_TYPE_JOINLEAVE == 2){
				e.setQuitMessage(Vars.LEAVE_MSG.replaceAll("%player%", UsefulUtils.GetNameWithColorsOnly(e.getPlayer(), true)));
				
			} else {
				e.setQuitMessage(Vars.LEAVE_MSG.replaceAll("%player%", e.getPlayer().getName()));
			}
		} else {
			e.setQuitMessage(null);
		}
		temp_ids.remove(e.getPlayer().getName());
		Vars.removeFromReplyList(e.getPlayer().getUniqueId());
		Vars.modifySocialSpyDisabledList(e.getPlayer(), false);
		
	}
	
	/*@EventHandler(priority = EventPriority.HIGHEST)
	public void onTagUpdate(AsyncPlayerReceiveNameTagEvent e){
		setupChat();
		String name = chat.getPlayerPrefix(e.getNamedPlayer());
		String s = ChatColor.getLastColors(ChatColor.translateAlternateColorCodes('&', name));
		e.setTag(s + e.getNamedPlayer().getName());
	}*/
	
	private boolean setupChat(){
		
		RegisteredServiceProvider<Chat> chatProvider = this.local.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }
        
        return (chat != null);
	}
	
	public static boolean isValueOnline(String pname){
		
		if (temp_ids.containsKey(pname)){
			return true;
		} else {
			return false;
		}
		
	}
	
	public static UUID getValueOnline(String pname){
		
		if (temp_ids.containsKey(pname)){
			
			return temp_ids.get(pname);
			
		} else {
			
			return null;
			
		}
				
	}
	
	
}
