package org.jkjkkj.TypicalChat;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.PluginCommand;

public class PermManager {

	private static Map<String, String> CMD_PERMS = new HashMap<String, String>();
	private static Map<String, String> OtherPerms = new HashMap<String, String>();

	public static void SetUpPerms(TypicalChat plugin){
		
		for (String s : plugin.getDescription().getCommands().keySet()){
			PluginCommand cmd = plugin.getCommand(s);
			if (cmd != null){
				cmd.setPermissionMessage(Vars.NO_PERM_MSG);
				CMD_PERMS.put(s, cmd.getPermission());
			}
		}
		
		OtherPerms.put("bypassignore", "chat.public.ignore.bypass");
		OtherPerms.put("unlimitedignore", "chat.public.ignore.nolimit");
		OtherPerms.put("bypassvanished", "chat.messaging.vanish.bypass");
		OtherPerms.put("joinleave", "chat.public.joinleavemsg");
		OtherPerms.put("socialspy", "chat.messaging.socialspy");
		OtherPerms.put("bypassspying", "chat.messaging.socialspy.bypass");
		OtherPerms.put("chatlockbypass", "chat.staff.chatlock.bypass");
		OtherPerms.put("canswear", "chat.public.allowswears");
		OtherPerms.put("chatlocknotify", "chat.staff.chatlock.notify");
		OtherPerms.put("chan-global", "chat.channel.global");
		OtherPerms.put("chan-vip", "chat.channel.vip");
		OtherPerms.put("chan-staff", "chat.channel.staff");
		OtherPerms.put("chan-builder", "chat.channel.builder");
		OtherPerms.put("silentchatextras", "chat.staff.silentchat");
		OtherPerms.put("who-staff", "chat.public.who.include");
		OtherPerms.put("who-owner", "chat.public.who.owner");
		OtherPerms.put("nonicknamesallowed", "chat.staff.nonicknames");
		
	}
	
	public static String getPermNode(String cmd){
		
		String s = "";
		if (CMD_PERMS.containsKey(cmd)){
			s = CMD_PERMS.get(cmd);
		}
		return s;
	}
	
	public static String getPerm(String input){
		String s = "";
		if (OtherPerms.containsKey(input)){
			s = OtherPerms.get(input);
		}
		return s;
	}
	
	
}
