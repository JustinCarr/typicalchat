package org.jkjkkj.TypicalChat.Commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.UsefulUtils;
import org.jkjkkj.TypicalChat.Vars;

public class PEmoteCMD implements CommandExecutor {

	private TCCommands local;
	
	public PEmoteCMD(TCCommands instance){
		
		this.local = instance;
		this.local.plugin.getCommand("pemote").setAliases(Arrays.asList("emote", "me"));
		
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("pemote") || label.equalsIgnoreCase("me") || label.equalsIgnoreCase("emote")){
						
			if (sender.hasPermission(PermManager.getPermNode("pemote"))){
				
				if (sender instanceof Player){
					
					Player p = (Player) sender;
					
					if (!Vars.CHAT_ENABLED){
						
						if (!p.hasPermission(PermManager.getPerm("chatlockbypass"))){
							
							p.sendMessage(Vars.NO_TALKING);
							return true;
						}
						
					}
					
					if (UsefulUtils.isMuted(p.getName())){
						p.sendMessage(Vars.NO_EMOTE_MUTED);
						return true;
					}
					
					if (args.length == 0){
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/me MESSAGE");
						return true;
					} else{
						
						StringBuilder sb = new StringBuilder();
						for (String s : args){
							
							sb.append(s).append(" ");
							
						}
						String pname = "";
						
						if (Vars.ME_NAME_TYPE == 3){
							pname = UsefulUtils.GetNameWithPrefix(p, true);
							
						} else if (Vars.ME_NAME_TYPE == 2){
							pname = UsefulUtils.GetNameWithColorsOnly(p, true);
							
						} else {
							pname = p.getName();
							
						}
						
						String tosend = Vars.ME_FORMAT.replaceAll("%player%", pname) + Vars.ME_COLOR + sb.toString().trim();
						
						for (Player temp : this.local.plugin.getServer().getOnlinePlayers()){
							
							temp.sendMessage(tosend);
							
						}
						
					}
					
				} else {
					
					if (args.length == 0){
						
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/me MESSAGE");
						return true;
						
					} else {
						
						StringBuilder sb = new StringBuilder();
						for (String s : args){
							
							sb.append(s).append(" ");
							
						}
						
						String tosend = Vars.ME_FORMAT.replaceAll("%player%", ChatColor.LIGHT_PURPLE + "Server") + Vars.ME_COLOR + sb.toString().trim();
						sender.sendMessage(tosend);
						
						for (Player temp : this.local.plugin.getServer().getOnlinePlayers()){
							
							temp.sendMessage(tosend);
							
						}
						
					}
					
				}
				
			}
			
			return true;
			
		}
		
		return false;
	}

}
