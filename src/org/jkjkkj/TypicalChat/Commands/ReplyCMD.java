package org.jkjkkj.TypicalChat.Commands;

import java.util.Arrays;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.UsefulUtils;
import org.jkjkkj.TypicalChat.Vars;

public class ReplyCMD implements CommandExecutor {
	
	TCCommands local;
	
	public ReplyCMD(TCCommands inst){
		
		this.local = inst;
		this.local.plugin.getCommand("reply").setAliases(Arrays.asList("r"));
		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("reply") || label.equalsIgnoreCase("r")){
			
			if (sender.hasPermission(PermManager.getPermNode("reply"))){
				
				if (!(sender instanceof Player)){
					
					sender.sendMessage(Vars.NOT_IN_CONSOLE);
					return true;
					
				}
				
				Player p = (Player) sender;
				
				if (args.length == 0){
					
					p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/r MESSAGE");
					return true;
					
				} else if (!Vars.CHAT_ENABLED){
					
					p.sendMessage(Vars.NO_TALKING);
				
				} else if (UsefulUtils.isMuted(p.getName())){
					
					p.sendMessage(Vars.NO_SEND_MUTED);
					return true;
					
				} else if (Vars.USE_SWEARS.contains(p.getName())){
					
					p.sendMessage(Vars.NO_TALKING_SWORE);
					return true;
					
				} else if (!Vars.chatIsOn(p.getUniqueId())){
					
					p.sendMessage(Vars.Ntrl + "Oops. You cannot talk because you are in silent mode.");
					p.sendMessage(Vars.Ntrl + "Disable silentmode by: " + Vars.Neg + "/silentchat off");
					return true;
					
				}
				

				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < args.length; i++){
					sb.append(args[i]).append(" ");
				
				}
				String msg = sb.toString().trim();
				
				String temp = UsefulUtils.GetWordsNotAllowed(p, msg);
				if (temp != null){
					
					p.sendMessage(Vars.Ntrl + "Oops. You cannot use the word '" + Vars.Neg + temp + Vars.Ntrl + "' in private messages!");
					p.sendMessage(Vars.Ntrl + "You have been muted for " + Vars.Neg + 10 + Vars.Ntrl + " seconds.");
					Vars.USE_SWEARS.add(p.getName());
					final String n = p.getName();
					
					this.local.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.local.plugin, new BukkitRunnable(){
						public void run(){
							Vars.USE_SWEARS.remove(n);
						}
					}, 200L);
					return true;
				
				}
				
				String s = Vars.getToReplyTo(p.getUniqueId());
				
				if (s == null){
					
					p.sendMessage(Vars.Ntrl + "Oops. You have no one to reply to!");
					
				} else if (s.equals("Server")){
					
					String psender = "";
					if (Vars.NAME_TYPE_MESSAGING == 2){
						
						psender = UsefulUtils.GetNameWithColorsOnly(p, true);
						
					} else if (Vars.NAME_TYPE_MESSAGING == 3){
						
						psender = UsefulUtils.GetNameWithPrefix(p, true);
						
					} else {
						psender = p.getName();
					}
					
					ConsoleCommandSender ccs = this.local.plugin.getServer().getConsoleSender();
					ccs.sendMessage(Vars.INCOMING_MSG.replaceAll("%sender%", psender).replaceAll("%message%", msg));
					Vars.setToReplyTo(p.getUniqueId(), "Server", true);
					return true;
					
				} else {
					
					UUID uuid = null;
					try {
						
						uuid = UUID.fromString(s);
						
					} catch (Exception e){
						
						Vars.removeFromReplyList(p.getUniqueId());
						p.sendMessage(Vars.Ntrl + "Oops. You have no one to reply to!");
						return true;
						
					}
					
					
					
					if (!this.local.plugin.getServer().getOfflinePlayer(uuid).isOnline()){
						
						p.sendMessage(Vars.Ntrl + "Oops. The player you last replied to is now offline!");
						return true;
						
					}
					
					Player receiver = this.local.plugin.getServer().getPlayer(uuid);
					if (UsefulUtils.IsVanished(receiver.getName())){
						
						if (!sender.hasPermission(PermManager.getPerm("bypassvanished"))){
							sender.sendMessage(Vars.NOT_ONLINE.replaceAll("%player%", receiver.getName()));
							return true;
						}
						
					}
					if (UsefulUtils.isMuted(receiver.getName())){
						p.sendMessage(Vars.Ntrl + "NOTICE: The player you are messaging is muted and cannot respond!");
					
					} else if (!Vars.chatIsOn(receiver.getUniqueId())){
						
						p.sendMessage(Vars.Ntrl + "Oops. You cannot message that player because they're in silent mode!");
						return true;
						
					}
					
					String psender = "";
					String preceiver = "";
					if (Vars.NAME_TYPE_MESSAGING == 2){
						psender = UsefulUtils.GetNameWithColorsOnly(p, true);
						preceiver = UsefulUtils.GetNameWithColorsOnly(receiver, true);
					} else if (Vars.NAME_TYPE_MESSAGING == 3){
						psender = UsefulUtils.GetNameWithPrefix(p, true);
						preceiver = UsefulUtils.GetNameWithPrefix(receiver, true);
					} else {
						psender = p.getName();
						preceiver = receiver.getName();
					}
					
					p.sendMessage(Vars.OUTGOING_MSG.replaceAll("%receiver%", preceiver).replaceAll("%message%", msg));
					Vars.setToReplyTo(p.getUniqueId(), receiver.getUniqueId().toString(), false);
					Vars.setToReplyTo(receiver.getUniqueId(), p.getUniqueId().toString(), false);
					receiver.sendMessage(Vars.INCOMING_MSG.replaceAll("%sender%", psender).replaceAll("%message%", msg));
					
					if (!p.hasPermission("chat.messaging.socialspy.bypass") && !receiver.hasPermission("chat.messaging.socialspy.bypass")){
						
						String node = PermManager.getPerm("socialspy");
						String dontsendto = p.getName();
						
						for (Player on : this.local.plugin.getServer().getOnlinePlayers()){
							
							if (on.hasPermission(node)){
								
								if (!on.getName().equals(dontsendto) && Vars.hasSocialSpyEnabled(p)){
									on.sendMessage(Vars.SPYING_MSG.replaceAll("%sender%", psender).replaceAll("%receiver%", preceiver).replaceAll("%message%", msg));

								}
							}
						}
					}
					
				}
				
			}
			
			return true;
			
		}
		
		return false;
	}

}
