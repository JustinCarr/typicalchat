package org.jkjkkj.TypicalChat.Commands;

import java.util.Arrays;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.TypicalChat.JoinLeaveHandler;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.UsefulUtils;
import org.jkjkkj.TypicalChat.Vars;

public class PrivateMessageCMD implements CommandExecutor {
	
	private TCCommands local;

	public PrivateMessageCMD(TCCommands input){
		this.local = input;
		this.local.plugin.getCommand("privatemessage").setAliases(Arrays.asList("msg", "w", "whisper", "tell", "pm", "t", "etell", "ewhisper", "emsg", "m"));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {
		String la = l.toLowerCase();
		if (la.equals("privatemessage") || la.equals("msg") || la.equals("w") || la.equals("whisper") || la.equals("tell") || la.equals("pm") || la.equals("t") || la.equals("etell") || la.equals("ewhisper") || la.equals("emsg") || la.equals("m")){
			
			if (sender.hasPermission(PermManager.getPermNode("privatemessage"))){
				
				Boolean tellthatotherscantrespond = false;
				if (!Vars.CHAT_ENABLED){
					if (!sender.hasPermission(PermManager.getPerm("chatlockbypass"))){
						sender.sendMessage(Vars.NO_TALKING);
						return true;
					} else {
						tellthatotherscantrespond = true;
					}
				}
				
				
				if (args.length < 2){
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect usage. Try: " + Vars.Neg + "/msg PLAYER MESSAGE");
				} else {
					
					if (sender instanceof Player){
						
						Player p = (Player) sender;
						
						if (!Vars.chatIsOn(p.getUniqueId())){
							
							p.sendMessage(Vars.Ntrl + "Oops. You cannot talk because you are in silent mode.");
							p.sendMessage(Vars.Ntrl + "Disable silentmode by: " + Vars.Neg + "/silentchat off");
							return true;
							
						}
						
						if (args[0].equals("Server")){
							
							if (UsefulUtils.isMuted(p.getName())){
								
								p.sendMessage(Vars.NO_SEND_MUTED);
								return true;
								
							} else if (Vars.USE_SWEARS.contains(p.getName())){
								
								p.sendMessage(Vars.NO_TALKING_SWORE);
								return true;
								
							}
							
							StringBuilder sb = new StringBuilder();
							for (int i = 1; i < args.length; i++){
								sb.append(args[i]).append(" ");
							
							}
							String msg = sb.toString().trim();
							
							String temp = UsefulUtils.GetWordsNotAllowed(p, msg);
							if (temp != null){
								
								p.sendMessage(Vars.Ntrl + "Oops. You cannot use the word '" + Vars.Neg + temp + Vars.Ntrl + "' in private messages!");
								p.sendMessage(Vars.Ntrl + "You have been muted for " + Vars.Neg + 10 + Vars.Ntrl + " seconds.");
								Vars.USE_SWEARS.add(p.getName());
								final String n = p.getName();
								
								this.local.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.local.plugin, new BukkitRunnable(){
									public void run(){
										Vars.USE_SWEARS.remove(n);
									}
								}, 200L);
								return true;
							
							}
							p.sendMessage(Vars.OUTGOING_MSG.replace("%receiver%", "Server").replace("%message%", msg));

							String psender = "";
							if (Vars.NAME_TYPE_MESSAGING == 2){
								
								psender = UsefulUtils.GetNameWithColorsOnly(p, true);
								
							} else if (Vars.NAME_TYPE_MESSAGING == 3){
								
								psender = UsefulUtils.GetNameWithPrefix(p, true);
								
							} else {
								psender = p.getName();
							}
							
							ConsoleCommandSender ccs = this.local.plugin.getServer().getConsoleSender();
							ccs.sendMessage(Vars.INCOMING_MSG.replace("%sender%", psender).replace("%message%", msg));
							Vars.setToReplyTo(p.getUniqueId(), "Server", true);
							return true;
							
						} else if (args[0].equals(p.getName())){
							
							p.sendMessage(Vars.Ntrl + "Oops. Why do you want to message yourself?!");
							return true;
							
						} else if (!JoinLeaveHandler.isValueOnline(args[0])){
							
							sender.sendMessage(Vars.NOT_ONLINE.replace("%player%", args[0]));
							return true;
							
						} else if (UsefulUtils.IsVanished(args[0])){
							
							if (!sender.hasPermission(PermManager.getPerm("bypassvanished"))){
								sender.sendMessage(Vars.NOT_ONLINE.replace("%player%", args[0]));
								return true;
							}
							
						} else {
							
							UUID uuid = JoinLeaveHandler.getValueOnline(args[0]);
							Player receiver = this.local.plugin.getServer().getPlayer(uuid);
							Boolean receivermuted = false;
							
							if (Vars.getIgnoredAsUUIDS(receiver.getUniqueId()).contains(p.getUniqueId())){
								
								if (!sender.hasPermission(PermManager.getPerm("chat.public.ignore.bypass"))){
									sender.sendMessage(Vars.Ntrl + "Oops. You cannot message " + Vars.Neg + args[0] + Vars.Ntrl + " because they are ignoring you!");
									return true;
								}
								
							} else if (UsefulUtils.isMuted(p.getName())){
								sender.sendMessage(Vars.NO_SEND_MUTED);
								return true;
							
							} else if (UsefulUtils.isMuted(args[0])){
								receivermuted = true;
							
							} else if (!Vars.chatIsOn(receiver.getUniqueId())){
								
								p.sendMessage(Vars.Ntrl + "Oops. You cannot message that player because they're in silent mode!");
								return true;
								
							}
							
							StringBuilder sb = new StringBuilder();
							for (int i = 1; i < args.length; i++){
								sb.append(args[i]).append(" ");
							
							}
							String msg = sb.toString().trim();
							
							String temp = UsefulUtils.GetWordsNotAllowed(p, msg);
							if (temp != null){
								
								p.sendMessage(Vars.Ntrl + "Oops. You cannot use the word '" + Vars.Neg + temp + Vars.Ntrl + "' in private messages!");
								p.sendMessage(Vars.Ntrl + "You have been muted for " + Vars.Neg + 10 + Vars.Ntrl + " seconds.");
								Vars.USE_SWEARS.add(p.getName());
								final String n = p.getName();
								
								this.local.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.local.plugin, new BukkitRunnable(){
									public void run(){
										Vars.USE_SWEARS.remove(n);
									}
								}, 200L);
								return true;
							
							}
							
							String psender = "";
							String preceiver = "";
							if (Vars.NAME_TYPE_MESSAGING == 2){
								psender = UsefulUtils.GetNameWithColorsOnly(p, true);
								preceiver = UsefulUtils.GetNameWithColorsOnly(receiver, true);
							} else if (Vars.NAME_TYPE_MESSAGING == 3){
								psender = UsefulUtils.GetNameWithPrefix(p, true);
								preceiver = UsefulUtils.GetNameWithPrefix(receiver, true);
							} else {
								psender = p.getName();
								preceiver = receiver.getName();
							}
							
							
							if (tellthatotherscantrespond){
								p.sendMessage(Vars.Ntrl + "NOTICE: The player may not be able to respond, as currently chat is " + Vars.Neg + "OFF" + Vars.Ntrl + ".");
							
							} else if (receivermuted){
								p.sendMessage(Vars.Ntrl + "NOTICE: The player you are messaging is muted and cannot respond!");
							
							}
							
							p.sendMessage(Vars.OUTGOING_MSG.replace("%receiver%", preceiver).replace("%message%", msg));
							Vars.setToReplyTo(p.getUniqueId(), receiver.getUniqueId().toString(), false);
							Vars.setToReplyTo(receiver.getUniqueId(), p.getUniqueId().toString(), false);
							receiver.sendMessage(Vars.INCOMING_MSG.replace("%sender%", psender).replace("%message%", msg));
							
							
							if (!p.hasPermission("chat.messaging.socialspy.bypass") && !receiver.hasPermission("chat.messaging.socialspy.bypass")){
								
								String node = PermManager.getPerm("socialspy");
								String dontsendto = p.getName();
								
								for (Player on : this.local.plugin.getServer().getOnlinePlayers()){
									
									if (on.hasPermission(node)){
										
										if (!on.getName().equals(dontsendto) && Vars.hasSocialSpyEnabled(p)){
											on.sendMessage(Vars.SPYING_MSG.replace("%sender%", psender).replace("%receiver%", preceiver).replace("%message%", msg));

										}
									}
								}
							}
							
						}
						
					} else {
						
						if (args[0].equals("Server")){
							
							sender.sendMessage(Vars.Ntrl + "Oops. Why would you want to message yourself?!");
							return true;
							
						} else if (!JoinLeaveHandler.isValueOnline(args[0])){
							
							sender.sendMessage(Vars.NOT_ONLINE.replace("%player%", args[0]));
							return true;
							
						}
						
						UUID uuid = JoinLeaveHandler.getValueOnline(args[0]);
						Player receiver = this.local.plugin.getServer().getPlayer(uuid);						
						ConsoleCommandSender ccs = this.local.plugin.getServer().getConsoleSender();
						
						StringBuilder sb = new StringBuilder();
						for (int i = 1; i < args.length; i++){
							sb.append(args[i]).append(" ");
						}
						
						if (UsefulUtils.isMuted(args[0])){
							ccs.sendMessage(Vars.Ntrl + "Notice: The player you are messaging is muted and cannot respond!");
						}
						
						String preceiver = UsefulUtils.GetNameWithColorsOnly(receiver, true);
						ccs.sendMessage(Vars.OUTGOING_MSG.replace("%receiver%", preceiver).replace("%message%", sb.toString().trim()));
						receiver.sendMessage(Vars.INCOMING_MSG.replace("%sender%", "Server").replace("%message%", sb.toString().trim()));
						receiver.sendMessage(Vars.Ntrl + "Message received by internal system. Send any responses to \"" + Vars.Neg + "Server" + Vars.Ntrl + "\"");
						
						Vars.setToReplyTo(receiver.getUniqueId(), "Server", true);
						
					}
					
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	
	
}
