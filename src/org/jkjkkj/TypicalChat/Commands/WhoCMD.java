package org.jkjkkj.TypicalChat.Commands;

import java.util.ArrayList;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.UsefulUtils;
import org.jkjkkj.TypicalChat.Vars;

public class WhoCMD implements CommandExecutor {
	
	private static Chat chat = null;
	private TCCommands local;
	
	public WhoCMD(TCCommands inst){
		
		this.local = inst;
		
	}
	

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("who")){
			
			if (sender.hasPermission(PermManager.getPermNode("who"))){
						
				Player[] playerz = Bukkit.getOnlinePlayers();
				int online = playerz.length;
				int staffonline = UsefulUtils.getStaff().size();
				int max = Bukkit.getMaxPlayers();
				
				
				ArrayList<Player> players = UsefulUtils.getStaff();
				StringBuilder sb = new StringBuilder();
				String comma = "";
				String list = "";
				setupChat();
				if (!players.isEmpty()){
					for (Player p : players){
						
						
						sb.append(comma).append(ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(p)) + p.getName());
						comma = Vars.WHO_COMMA_COLOR + ", ";
					}
					list = ChatColor.WHITE + sb.toString().trim();
				} else {
					list = Vars.WHO_COMMA_COLOR + "None";
				}
				
				ArrayList<String> tosend = new ArrayList<String>();
				for (String s : Vars.WHO_MESSAGE){
					tosend.add(s.replaceAll("%ONLINE%", online + "").replaceAll("%MAX%", max + "").replaceAll("%STAFF%", staffonline + "").replaceAll("%LIST%", list));
					
				}
				
				for (String s : tosend){
					sender.sendMessage(s);
				}
				
			}
			
			return true;
			
		}

		return false;
	}
	
    private boolean setupChat(){
        RegisteredServiceProvider<Chat> chatProvider = this.local.plugin.getServer().getServicesManager().getRegistration(Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

}
