package org.jkjkkj.TypicalChat.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.Vars;
import org.jkjkkj.TypicalChat.Broadcast.Broadcaster;

public class TypicalChatCMD implements CommandExecutor {
	
	private String r = ChatColor.RED + "";
	private String w = ChatColor.WHITE + "";
	private TCCommands local;
	
	public TypicalChatCMD(TCCommands inst){
		this.local = inst;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("typicalchat")){
			if (sender.hasPermission(PermManager.getPermNode("typicalchat"))){
				if (args.length == 0){
					
					sender.sendMessage(r + "==================================================");
					sender.sendMessage(ChatColor.GRAY + "Here's how to use the '/typicalchat' command...");
					sender.sendMessage(r + "/typicalchat help " + w + "See this help menu");
					sender.sendMessage(r + "/typicalchat info " + w + "Get info on this chat software");
					sender.sendMessage(r + "/typicalchat reload config " + w + "Reload the config");
					sender.sendMessage(r + "/typicalchat reload players " + w + "Reload the players config");
					sender.sendMessage(r + "/typicalchat reload tcx_config " + w + "Reload TCExtras config");
					sender.sendMessage(r + "/typicalchat reload broadcasts " + w + "Reload the broadcasts");
					sender.sendMessage(r + "/typicalchat reload all " + w + "Reload both files");
					sender.sendMessage(r + "==================================================");

				} else if (args[0].equalsIgnoreCase("?") || args[0].equalsIgnoreCase("help")){
					sender.sendMessage(r + "==================================================");
					sender.sendMessage(ChatColor.GRAY + "Here's how to use the '/typicalchat' command...");
					sender.sendMessage(r + "/typicalchat help " + w + "See this help menu");
					sender.sendMessage(r + "/typicalchat info " + w + "Get info on this chat software");
					sender.sendMessage(r + "/typicalchat reload config " + w + "Reload the config");
					sender.sendMessage(r + "/typicalchat reload players " + w + "Reload the players config");
					sender.sendMessage(r + "/typicalchat reload tcx_config " + w + "Reload TCExtras config");
					sender.sendMessage(r + "/typicalchat reload broadcasts " + w + "Reload the broadcasts");
					sender.sendMessage(r + "/typicalchat reload all " + w + "Reload both files");
					sender.sendMessage(r + "==================================================");
				} else if (args[0].equalsIgnoreCase("reload")){
					
					if (args.length != 2){
						
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect usage. Specify what you want to reload!");
						sender.sendMessage(Vars.Ntrl + "'" + Vars.Neg + "config" + Vars.Ntrl + "', '" + Vars.Neg + "broadcasts" + Vars.Ntrl + "', or '" + Vars.Neg + "all" + Vars.Ntrl + "'");
						
					} else if (args[1].equalsIgnoreCase("config")){
						
						Vars.ReloadFiles(false, false);
						Vars.GetVars(false);
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "The " + Vars.Pos + "config" + Vars.Ntrl + " file has been reloaded!");
						
					} else if (args[1].equalsIgnoreCase("broadcasts")){
						
						Broadcaster.reSetUpBroadcasts();
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "The " + Vars.Pos + "broadcasts" + Vars.Ntrl + " file has been reloaded!");
						
					} else if (args[1].equalsIgnoreCase("players") || args[1].equalsIgnoreCase("player_data")){
						
						Vars.ReloadFiles(true, false);
						Vars.GetVars(true);
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "The " + Vars.Pos + "player_data" + Vars.Ntrl + " file has been reloaded!");
						
					} else if (args[1].equalsIgnoreCase("tcx_config") || args[1].equalsIgnoreCase("tcextras")){
						
						Vars.ReloadFiles(true, true);
						Vars.GetVarsFromTCXConfig();
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "The " + Vars.Pos + "tcx_config" + Vars.Ntrl + " file has been reloaded!");
						
					} else if (args[1].equalsIgnoreCase("all") || args[1].equalsIgnoreCase("both")){
						
						Vars.ReloadFiles(true, false);
						Vars.GetVars(true);
						Vars.ReloadFiles(false, false);
						Vars.GetVars(false);
						Vars.ReloadFiles(true, true);
						Vars.GetVarsFromTCXConfig();
						Broadcaster.reSetUpBroadcasts();
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Pos + "ALL" + Vars.Ntrl + " files have been reloaded!");

						
					} else {
						
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect usage. Specify what you want to reload!");
						sender.sendMessage(Vars.Ntrl + "'" + Vars.Neg + "config" + Vars.Ntrl + "', '" + Vars.Neg + "broadcasts" + Vars.Ntrl + "', or '" + Vars.Neg + "all" + Vars.Ntrl + "'");
						
					}
					
				} else if (args[0].equalsIgnoreCase("info")){
					
					String version = this.local.plugin.getDescription().getVersion();
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "TypicalChat (" + Vars.Pos + "v" + version + Vars.Ntrl + ") custom made for TypicalCraft"
							+ " by " + Vars.Pos + "idr" + "um69");
					
				} else {
					
					sender.sendMessage(r + "Oops. Incorrect Usage. Get help: " + Vars.Neg + "/typicalchat help");
					
				}
				
				return true;
			}
			return true;
		}
		
		return false;
	}
	
	
	
}
