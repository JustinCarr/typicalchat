package org.jkjkkj.TypicalChat.Commands;

import java.util.Arrays;

import org.jkjkkj.TypicalChat.TypicalChat;

public class TCCommands {

	public TypicalChat plugin;
	
	public TCCommands(TypicalChat inst){
		this.plugin = inst;
		
		this.plugin.getServer().getPluginManager().registerEvents(new PreProcessListener(this.plugin), this.plugin);
		this.plugin.getCommand("togglechat").setExecutor(new ToggleChatCMD(this));
		this.plugin.getCommand("privatemessage").setExecutor(new PrivateMessageCMD(this));
		this.plugin.getCommand("typicalchat").setExecutor(new TypicalChatCMD(this));
		this.plugin.getCommand("channel").setExecutor(new ChannelCMD(this));
		this.plugin.getCommand("ignore").setExecutor(new IgnoreCMD(this));
		this.plugin.getCommand("pemote").setExecutor(new PEmoteCMD(this));
		this.plugin.getCommand("clearchat").setExecutor(new ClearChatCMD(this));
		this.plugin.getCommand("silentchat").setExecutor(new SilentChatCMD(this));
		this.plugin.getCommand("who").setExecutor(new WhoCMD(this));
		this.plugin.getCommand("reply").setExecutor(new ReplyCMD(this));
		this.plugin.getCommand("socialspy").setExecutor(new SocialSpyCMD());
		StaffVotesLoveCMD svl_cmd = new StaffVotesLoveCMD(this);
		for (String s : Arrays.asList("staff", "votes", "love")){
			
			this.plugin.getCommand(s).setExecutor(svl_cmd);
			
		}
		
	}
	
	
}
