package org.jkjkkj.TypicalChat.Commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.TypicalChat.JoinLeaveHandler;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.UUIDManager;
import org.jkjkkj.TypicalChat.Vars;

public class SilentChatCMD implements CommandExecutor {
	
	private String g = ChatColor.GOLD + "";
	private String w = ChatColor.WHITE + "";
	private String i = ChatColor.ITALIC + "";
	private ArrayList<String> help_msg_norm = new ArrayList<String>();
	private ArrayList<String> help_msg_mod = new ArrayList<String>();
	
	public SilentChatCMD(TCCommands inst){
		
		help_msg_norm.add(ChatColor.AQUA + "==================================================");
		help_msg_norm.add(ChatColor.GRAY + "Here's how to use the '/silentchat' command...");
		help_msg_norm.add(g + "/silentchat help " + w + "View this help message");
		help_msg_norm.add(g + "/silentchat on " + w + "Turn on silent chat. You will no longer receive chat or broadcasts.");
		help_msg_norm.add(g + "/silentchat off " + w + "Turn off silent chat. You will again receive chat and broadcasts.");
		help_msg_norm.add(g + "/silentchat check " + w + "Check if you're on silent mode.");
		help_msg_norm.add(g + "/silentchat check PLAYER " + w + "Check a player's silent mode.");
		help_msg_norm.add(ChatColor.AQUA + "==================================================");

		help_msg_mod.add(ChatColor.AQUA + "==================================================");
		help_msg_mod.add(ChatColor.GRAY + "Here's how to use the '/silentchat' command...");
		help_msg_mod.add(g + "/silentchat help " + w + "View this help message");
		help_msg_mod.add(g + "/silentchat on " + w + "Turn on silent chat. You will no longer receive chat or broadcasts.");
		help_msg_mod.add(g + "/silentchat off " + w + "Turn off silent chat. You will again receive chat and broadcasts.");
		help_msg_mod.add(g + "/silentchat check " + w + "Check if you're on silent mode.");
		help_msg_mod.add(g + "/silentchat check PLAYER " + w + "Check a player's silent mode.");
		help_msg_mod.add(g + i + "/silentchat forceon PLAYER " + w + "Force a player into silent mode.");
		help_msg_mod.add(g + i + "/silentchat forceoff PLAYER " + w + "Force a player out of silent mode.");
		help_msg_mod.add(g + i + "/silentchat list " + w + "List all players on silent mode.");
		help_msg_mod.add(ChatColor.AQUA + "==================================================");
		
	}
	
	public static boolean isPlayerSilentStatusOn(Player p){
		
		if (Vars.chatIsOn(p.getUniqueId())){
			return false;
		} else {
			return true;
		}
		
	}
	
	public static void togglePlayerSilentStatus(Player p, boolean EnableSilentMode){
		
		if (EnableSilentMode){
			
			if (Vars.chatIsOn(p.getUniqueId())){
				
				Vars.setChat(p.getUniqueId(), p.getName(), false);
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Silent mode is now " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "This means your chat is now " + Vars.Neg + "OFF" + Vars.Ntrl + "!");
				
				
			} else {
				
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You already have silent mode " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
				
			}
			
		} else {
			
			if (!Vars.chatIsOn(p.getUniqueId())){
				
				Vars.setChat(p.getUniqueId(), p.getName(), true);
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Silent mode is now " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "This means your chat is now " + Vars.Pos + "ON" + Vars.Ntrl + "!");
				
			} else {
				
				p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You already have silent mode " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
				
			}
			
		}
		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("silentchat")){
						
			if (sender.hasPermission(PermManager.getPermNode("silentchat"))){
				
				if (args.length == 0){
					
					if (sender.hasPermission(PermManager.getPerm("silentchatextras"))){
						
						for (String s : help_msg_mod){
							sender.sendMessage(s);
						}
						
					} else {
						
						for (String s : help_msg_norm){
							sender.sendMessage(s);
						}
						
					}
					
				} else if (args[0].equalsIgnoreCase("help")){
					
					if (sender.hasPermission(PermManager.getPerm("silentchatextras"))){
						
						for (String s : help_msg_mod){
							sender.sendMessage(s);
						}
						
					} else {
						
						for (String s : help_msg_norm){
							sender.sendMessage(s);
						}
						
					}
					
				} else if (args[0].equalsIgnoreCase("on") || args[0].equalsIgnoreCase("enable")){
					
					if (!(sender instanceof Player)){
						
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
						
					}
					Player p = (Player) sender;
					
					togglePlayerSilentStatus(p, true);
					
				} else if (args[0].equalsIgnoreCase("off") || args[0].equalsIgnoreCase("disable")){
					
					if (!(sender instanceof Player)){
						
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
						
					}
					Player p = (Player) sender;
					
					togglePlayerSilentStatus(p, false);
					
				} else if (args[0].equalsIgnoreCase("check")){
					
					if (args.length == 1){
						
						if (!(sender instanceof Player)){
							
							sender.sendMessage(Vars.NOT_IN_CONSOLE);
							return true;
							
						}
						Player p = (Player) sender;
						
						if (Vars.chatIsOn(p.getUniqueId())){
							sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You have silent mode " + Vars.Neg + "DISABLED" + Vars.Ntrl + " (Your chat is " + Vars.Pos + "ON" + Vars.Ntrl + ")");
						} else {
							sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You have silent mode " + Vars.Pos + "ENABLED" + Vars.Ntrl + " (Your chat is " + Vars.Neg + "OFF" + Vars.Ntrl + ")");
						}
						
					} else {
						
						UUID uuid = null;
						if (JoinLeaveHandler.isValueOnline(args[1])){
							
							uuid = JoinLeaveHandler.getValueOnline(args[1]);
							
						} else {
							
							sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Checking player. Please wait...");
							
							try {
								
								uuid = UUIDManager.getUUIDOf(args[1]);
								
							} catch (Exception e){
								
								uuid = null;
								
							}
							
						}
						
						if (uuid != null){
							
							if (Vars.chatIsOn(uuid)){
								sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + args[1] + " has silent mode " + Vars.Neg + "DISABLED" + Vars.Ntrl + " (Their chat is " + Vars.Pos + "ON" + Vars.Ntrl + ")");

							} else {
								sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + args[1] + " has silent mode " + Vars.Pos + "ENABLED" + Vars.Ntrl + " (Their chat is " + Vars.Neg + "OFF" + Vars.Ntrl + ")");
							}
							
						} else {
							sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + args[1] + " has silent mode " + Vars.Neg + "DISABLED" + Vars.Ntrl + " (Their chat is " + Vars.Pos + "ON" + Vars.Ntrl + ")");
						}
						
					}
					
				} else if (args[0].equalsIgnoreCase("forceon")){
					
					if (!sender.hasPermission(PermManager.getPerm("silentchatextras"))){
						
						sender.sendMessage(Vars.Ntrl + "Oops. You aren't allowed to do this!");
						return true;
						
					}
					
					if (args.length != 2){
						
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/silentchat forceon PLAYER");
						return true;
						
					}
					
					UUID uuid = null;
					if (JoinLeaveHandler.isValueOnline(args[1])){
						
						uuid = JoinLeaveHandler.getValueOnline(args[1]);
						
					} else {
						
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Checking player. Please wait...");
						
						try {
							
							uuid = UUIDManager.getUUIDOf(args[1]);
							
						} catch (Exception e){
							
							uuid = null;
							
						}
						
					}
					
					if (uuid == null){
						
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Neg + args[1] + Vars.Ntrl + " is not an existing minecraft player!");
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Maybe they've recently changed their name to something new!");
						return true;
						
					}
					
					if (Vars.chatIsOn(uuid)){
						Vars.setChat(uuid, args[0], false);
					}
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Player " + args[1] + " has been forced into silent mode.");
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "...Meaning their chat is now " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
					
				} else if (args[0].equalsIgnoreCase("forceoff")){
					
					if (!sender.hasPermission(PermManager.getPerm("silentchatextras"))){
						
						sender.sendMessage(Vars.Ntrl + "Oops. You aren't allowed to do this!");
						return true;
						
					}
					
					if (args.length != 2){
						
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/silentchat forceoff PLAYER");
						return true;
						
					}
					
					UUID uuid = null;
					if (JoinLeaveHandler.isValueOnline(args[1])){
						
						uuid = JoinLeaveHandler.getValueOnline(args[1]);
						
					} else {
						
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Checking player. Please wait...");
						
						try {
							
							uuid = UUIDManager.getUUIDOf(args[1]);
							
						} catch (Exception e){
							
							uuid = null;
							
						}
						
					}
					
					if (uuid == null){
						
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + args[1] + " is not an existing minecraft player!");
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Maybe they've recently changed their name to something new!");
						return true;
						
					}
					
					if (!Vars.chatIsOn(uuid)){
						Vars.setChat(uuid, args[1], true);
					}
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Player " + args[1] + " has been forced out of silent mode.");
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "...Meaning their chat is now " + Vars.Neg + "ENABLED" + Vars.Ntrl + "!");
					
				} else if (args[0].equalsIgnoreCase("list")){
					
					if (!sender.hasPermission(PermManager.getPerm("silentchatextras"))){
						
						sender.sendMessage(Vars.Ntrl + "Oops. You aren't allowed to do this!");
						return true;
						
					}
					
					ArrayList<String> chat_off = Vars.getAllWithChatOff();
					if (chat_off.isEmpty()){
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "There are currently no players in silent mode!");
					} else {
						
						StringBuilder sb = new StringBuilder();
						String comma = "";
						for (String s : chat_off){
							
							sb.append(comma).append(Vars.Pos + s);
							comma = Vars.Ntrl + ", ";
							
						}
						
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "There are " + Vars.Pos + chat_off.size() + Vars.Ntrl + " player(s) in silent mode!");
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "This includes: " + sb.toString().trim());
						
					}
					
				} else {
					
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect arguments. For help: " + Vars.Neg + "/silentchat help");
					
				}
				
			}
			
			return true;
		}
		
		return false;
	}

}
