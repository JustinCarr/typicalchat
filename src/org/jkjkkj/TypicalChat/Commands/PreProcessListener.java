package org.jkjkkj.TypicalChat.Commands;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Server;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.SimplePluginManager;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.TypicalChat;
import org.jkjkkj.TypicalChat.Vars;



public class PreProcessListener implements Listener {
	
	private SimpleCommandMap theMap = null;
	private TypicalChat plugin;
	
	public PreProcessListener(TypicalChat inst){
		this.plugin = inst;
		
		try {
			this.theMap = getCmdMap(this.plugin.getServer());
		} catch (Exception e) {
			TypicalChat.LogMessage("Error has occurred in grabbing Command Map", true);
			e.printStackTrace();
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPreProcess(PlayerCommandPreprocessEvent e){
		
		String initial = e.getMessage().split(" ")[0].toLowerCase();
		
		List<String> tochange = new ArrayList<String>();
		tochange.add("/msg");
		tochange.add("/w");
		tochange.add("/whisper");
		tochange.add("/tell");
		tochange.add("/pm");
		tochange.add("/t");
		tochange.add("/etell");
		tochange.add("/ewhisper");
		tochange.add("/emsg");
		tochange.add("/m");
		
		
		if (tochange.contains(initial)){
			
			String[] msg = e.getMessage().split(" ");
			StringBuilder sb = new StringBuilder();
			sb.append("privatemessage ");
			for (int i = 1; i < msg.length; i++){
				sb.append(msg[i]).append(" ");
			}
			e.setCancelled(true);
			e.getPlayer().performCommand(sb.toString().trim());
			
			
		} else if (initial.equalsIgnoreCase("/me")){
			
			String[] msg = e.getMessage().split(" ");
			StringBuilder sb = new StringBuilder();
			sb.append("pemote ");
			for (int i = 1; i < msg.length; i++){
				sb.append(msg[i]).append(" ");
			}
			e.setCancelled(true);
			e.getPlayer().performCommand(sb.toString().trim());
			
		} else if (initial.equalsIgnoreCase("/?")){
			
			if (!e.getPlayer().hasPermission(PermManager.getPerm("confidential"))){
				if (!e.getPlayer().getName().equals("idrum69")){
					e.getPlayer().sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Unknown Command. Type \"" + Vars.Pos + "/help" + Vars.Ntrl + "\" for help.");
			        e.getPlayer().sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Or... Visit us on the web ==> " + Vars.Neg + "typicalcraft.com");
			        e.setCancelled(true);
			        return;
				}
			}
			
		} else if (initial.equalsIgnoreCase("/version") || initial.equalsIgnoreCase("/plugins")){
			
			if (!e.getPlayer().hasPermission(PermManager.getPerm("confidential"))){
				if (!e.getPlayer().getName().equals("idrum69")){
					
					e.getPlayer().sendMessage(Vars.Ntrl + "Oops. This information is confidential!");
					e.setCancelled(true);
			        return;
				
				}
			}
			
		}
		
		if (!e.isCancelled()){
	    	
			
			String initialcmd = e.getMessage();
		    String[] cmd = null;
		    	
		    if(initialcmd.charAt(0) == '/'){
		    	initialcmd = initialcmd.replaceFirst("/", "");
		    }
		    
		    cmd = initialcmd.trim().split(" ");
		    if (!isCmdRegistered(cmd[0])){
		    	
		        e.getPlayer().sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Unknown Command. Type \"" + Vars.Pos + "/help" + Vars.Ntrl + "\" for help.");
		        e.getPlayer().sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Or... Visit us on the web ==> " + Vars.Neg + "typicalcraft.com");
		        e.setCancelled(true);
		        return;
		    }
		    	
		}
		
	}
	
	private SimpleCommandMap getCmdMap(Server serverObj) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
	    
		if ((serverObj.getPluginManager() instanceof SimplePluginManager)) {
	    	
		    Field field = SimplePluginManager.class.getDeclaredField("commandMap");
			field.setAccessible(true);
			return (SimpleCommandMap)field.get(serverObj.getPluginManager());
			
	    }
	    
        TypicalChat.LogMessage("Error has occurred in sending 'Unkown Command' message!", true);
	    return null;
	    
	}
	
	private boolean isCmdRegistered(String cmdname){
		return theMap.getCommand(cmdname) != null;
	}

}
