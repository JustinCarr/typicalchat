package org.jkjkkj.TypicalChat.Commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.Vars;
import org.jkjkkj.TypicalChat.ChatChannels.Channels;

public class ChannelCMD implements CommandExecutor {

	private TCCommands local;
	private String b = ChatColor.AQUA + "";
	private String g = ChatColor.GOLD + "";
	private String w = ChatColor.WHITE + "";
	private ArrayList<String> pages = new ArrayList<String>();
	private String d_blue = ChatColor.DARK_BLUE + "";
	private String d_green = ChatColor.DARK_GREEN + "";
	
	
	public ChannelCMD(TCCommands inst){
		this.local = inst;
		this.local.plugin.getCommand("channel").setAliases(Arrays.asList("ch", "chan"));
		pages.add(d_blue + ChatColor.BOLD + "Hey there, citizen of " + d_green + ChatColor.BOLD  + "TypicalCraft" + d_blue + ChatColor.BOLD  + "!\n" + Vars.Ntrl + "------------------\n\n" + ChatColor.BLACK + "Here's a simple guide on how to manage your life in chat channels...\n\nIf you are still bamboozled, hit us up on the forums!\n\n" + d_green + "< " + d_blue + ChatColor.UNDERLINE + "typicalcraft.com" + d_green + " >");
		pages.add(d_blue + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Tagging\n\n" + ChatColor.BLACK + "By typing an online player's name in chat, you are able to tag a player. The tagged name will then be colored accordingly, and the tagged player will be notified with a starred message.\n\nExamples on next page.");
		pages.add(ChatColor.BLACK + "" + ChatColor.UNDERLINE + "\nEx. 1" + ChatColor.BLACK + " - hallowhead1\n\nI wanna tag player\n" + ChatColor.BLACK + "" + ChatColor.BOLD + "[OWNER]" + ChatColor.DARK_RED + " hallowhead1" + ChatColor.BLACK + "\nby saying \"Hi, hallowhead1!!\"\n\nTo me it will look like:\n\"" + ChatColor.GRAY 
				+ "Hi, " + ChatColor.DARK_RED + "hallowhead1" + ChatColor.GRAY + "!!" + ChatColor.BLACK + "\"\nTo him it will look like:\n\"" + ChatColor.GREEN + "*" + ChatColor.GRAY + "Hi, " + ChatColor.DARK_RED + "hallowhead1" + ChatColor.GRAY + "!!" + ChatColor.GREEN + "*" + ChatColor.BLACK + "\"");
		pages.add(ChatColor.BLACK + "" + ChatColor.UNDERLINE + "\nEx. 2" + ChatColor.BLACK + " - idrum69\n\nI wanna tag player\n" + ChatColor.LIGHT_PURPLE + "[Developer]" + ChatColor.RED + "idrum69" + ChatColor.BLACK + "\nby saying \"Sup, idrum69???\"\n\nTo me it will look like:\n\"" + ChatColor.GRAY 
				+ "Sup, " + ChatColor.RED + "idrum69" + ChatColor.GRAY + "???" + ChatColor.BLACK + "\"\nTo him it will look like:\n\"" + ChatColor.GREEN + "*" + ChatColor.GRAY + "Sup, " + ChatColor.RED + "idrum69" + ChatColor.GRAY + "???" + ChatColor.GREEN + "*" + ChatColor.BLACK + "\"");		
		pages.add(d_blue + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Channels\n\n" + ChatColor.BLACK + "" + ChatColor.ITALIC + "Okay, so if you're a normal civilian player of TC then you should be dedicated and cool so that you get a higher rank, as channels are really only useful to the higher ranks of staff and donator!\n       ----");
		pages.add(ChatColor.BLACK + "\nwork in progress");
		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {
		
		if (l.equalsIgnoreCase("channel") || l.equalsIgnoreCase("chan") || l.equalsIgnoreCase("ch")){
			
			if (sender.hasPermission(PermManager.getPermNode("channel"))){
				
				if (args.length == 0){
					
					DisplayHelpMSG(sender);
					
				} else if (args[0].equalsIgnoreCase("help")){
					
					DisplayHelpMSG(sender);
					
				} else if (args[0].equalsIgnoreCase("guide")){
					
					CreateGuideBook(sender);
					
				} else if (args[0].equalsIgnoreCase("list")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					
					Channels.doCheck(p);
					ArrayList<String> chans = new ArrayList<String>();
					chans.add("public");
					
					if (Channels.builderContains(p)){
						chans.add("builder");
					}
					if (Channels.staffContains(p)){
						chans.add("staff");
					}
					if (Channels.vipContains(p)){
						chans.add("vip");
					}
					if (Channels.globalContains(p)){
						chans.add("global");
					}
					
					StringBuilder sb = new StringBuilder();
					String comma = "";
					
					for (String c : chans){
						sb.append(comma).append(Vars.Pos + c.toUpperCase());
						comma = Vars.Ntrl + ", ";
					}
					
					p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You have access to " + Vars.Pos + chans.size() + Vars.Ntrl + " channels!");
					p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "These are: " + sb.toString());
					
				} else if (args[0].equalsIgnoreCase("active")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					
					String chan = Channels.getActiveChan(p.getName());
					
					p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are actively chatting in the " + Vars.Pos + chan.toUpperCase() + Vars.Ntrl + " channel!");
					
				} else if (args[0].equalsIgnoreCase("join") || args[0].equalsIgnoreCase("talk") ||
						args[0].equalsIgnoreCase("j")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					
					if (args.length <= 1){
						p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/chan join CHANNEL_NAME");
						return true;
					}
					
					String chan = "";
					
					if (args[1].startsWith("#")){
						chan = args[1].replace("#", "").toLowerCase();
					} else {
						chan = args[1].toLowerCase();
					}
					
					Channels.doCheck(p);
					
					if (chan.equals("public") || chan.equals("tc") || chan.equals("local")){
						
						if (Channels.getActiveChan(p.getName()).equals("public")){
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "public" + Vars.Ntrl + " channel!");
							return true;
						}
						
						Channels.setActiveChan(p.getName(), "public");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "public" + Vars.Ntrl + " channel!");
						
					} else if (chan.equals("staff") || chan.equals("s")){
						
						if (Channels.getActiveChan(p.getName()).equals("staff")){
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "staff" + Vars.Ntrl + " channel!");
							return true;
						}
						
						if (Channels.channelContainsPlayer("staff", p.getName())){
							Channels.setActiveChan(p.getName(), "staff");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "staff" + Vars.Ntrl + " channel!");
						} else {
							p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "staff" + Vars.Ntrl + "' channel");
						}
						
					} else if (chan.equals("vip") || chan.equals("donor") || chan.equals("v")){
						
						if (Channels.getActiveChan(p.getName()).equals("vip")){
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "vip" + Vars.Ntrl + " channel!");
							return true;
						}
						
						if (Channels.channelContainsPlayer("vip", p.getName())){
							Channels.setActiveChan(p.getName(), "vip");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "vip" + Vars.Ntrl + " channel!");
						} else {
							p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "vip" + Vars.Ntrl + "' channel");
						}
						
					} else if (chan.equals("builder") || chan.equals("b") || chan.equals("build")){
						
						if (Channels.getActiveChan(p.getName()).equals("builder")){
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "builder" + Vars.Ntrl + " channel!");
							return true;
						}
						
						if (Channels.channelContainsPlayer("builder", p.getName())){
							Channels.setActiveChan(p.getName(), "builder");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "builder" + Vars.Ntrl + " channel!");
						} else {
							p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "builder" + Vars.Ntrl + "' channel");
						}
						
					} else if (chan.equals("global") || chan.equals("g")){
						
						if (Channels.getActiveChan(p.getName()).equals("global")){
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "global" + Vars.Ntrl + " channel!");
							return true;
						}
						
						if (Channels.channelContainsPlayer("global", p.getName())){
							Channels.setActiveChan(p.getName(), "global");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "global" + Vars.Ntrl + " channel!");
						} else {
							p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "global" + Vars.Ntrl + "' channel");
						}
						
					} else {
						
						p.sendMessage(Vars.Ntrl + "Oops. The channel '" + Vars.Neg + chan + Vars.Ntrl + "' does not exist!");
						
					}
					
				} else if (args[0].equalsIgnoreCase("public") || args[0].equalsIgnoreCase("#public")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					Channels.doCheck(p);
					
					if (Channels.getActiveChan(p.getName()).equals("public")){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "public" + Vars.Ntrl + " channel!");
						return true;
					}
					
					Channels.setActiveChan(p.getName(), "public");
					p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "public" + Vars.Ntrl + " channel!");
					
					
				} else if (args[0].equalsIgnoreCase("vip") || args[0].equalsIgnoreCase("#vip")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					Channels.doCheck(p);
					
					if (Channels.getActiveChan(p.getName()).equals("vip")){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "vip" + Vars.Ntrl + " channel!");
						return true;
					}
					
					if (Channels.channelContainsPlayer("vip", p.getName())){
						Channels.setActiveChan(p.getName(), "vip");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "vip" + Vars.Ntrl + " channel!");
					} else {
						p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "vip" + Vars.Ntrl + "' channel");
					}
					
				} else if (args[0].equalsIgnoreCase("builder") || args[0].equalsIgnoreCase("#builder")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					Channels.doCheck(p);
					
					if (Channels.getActiveChan(p.getName()).equals("builder")){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "builder" + Vars.Ntrl + " channel!");
						return true;
					}
					
					if (Channels.channelContainsPlayer("builder", p.getName())){
						Channels.setActiveChan(p.getName(), "builder");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "builder" + Vars.Ntrl + " channel!");
					} else {
						p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "builder" + Vars.Ntrl + "' channel");
					}
					
				} else if (args[0].equalsIgnoreCase("staff") || args[0].equalsIgnoreCase("#staff")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					Channels.doCheck(p);
					
					if (Channels.getActiveChan(p.getName()).equals("staff")){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "staff" + Vars.Ntrl + " channel!");
						return true;
					}
					
					if (Channels.channelContainsPlayer("staff", p.getName())){
						Channels.setActiveChan(p.getName(), "staff");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "staff" + Vars.Ntrl + " channel!");
					} else {
						p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "staff" + Vars.Ntrl + "' channel");
					}
					
				} else if (args[0].equalsIgnoreCase("global") || args[0].equalsIgnoreCase("#global")){
					
					if (!(sender instanceof Player)){
						sender.sendMessage(Vars.NOT_IN_CONSOLE);
						return true;
					}
					
					Player p = (Player) sender;
					Channels.doCheck(p);
					
					if (Channels.getActiveChan(p.getName()).equals("global")){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are " + Vars.Neg +  "ALREADY" + Vars.Ntrl + " chatting in the " + Vars.Pos + "global" + Vars.Ntrl + " channel!");
						return true;
					}
					
					if (Channels.channelContainsPlayer("global", p.getName())){
						
						Channels.setActiveChan(p.getName(), "global");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now chatting in the " + Vars.Pos + "global" + Vars.Ntrl + " channel!");
					
					} else {
						
						p.sendMessage(Vars.Ntrl + "Oops. You don't have permission for the '" + Vars.Neg + "global" + Vars.Ntrl + "' channel");
						
					}
					
					
				} else if (args[0].equalsIgnoreCase("credits")){
					
					String version[] = this.local.plugin.getDescription().getVersion().split("-");
					
					sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Custom chat system by " + Vars.Pos + "i" + "dr" + "um69" + Vars.Ntrl + " for TypicalCraft.");
					if (version.length == 1){
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Version: " + Vars.Pos + version[0]);
					} else {
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Chat version: " + Vars.Pos + version[0]);
						sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Adapted for Minecraft version: " + Vars.Pos + version[1]);
					}
					
					
				} else {
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect usage. For help: " + Vars.Neg + "/chan help");
				}
				
			}
			
			return true;
			
		}
		
		return false;
	}
	
	public void DisplayHelpMSG(CommandSender sender){
		
		sender.sendMessage(b + "==================================================");
		sender.sendMessage(ChatColor.GRAY + "Here's how to use the '/channel' command...");
		sender.sendMessage(g + "/ch help " + w + "View this help message");
		sender.sendMessage(g + "/ch guide " + w + "Get a guide book on how to use chat channels");
		sender.sendMessage(g + "/ch list " + w + "List the channels you have access to");
		sender.sendMessage(g + "/ch active " + w + "Get the channel you're currently speaking in");
		sender.sendMessage(g + "/ch join CHANNEL_NAME" + w + "Change the channel you're speaking in");
		sender.sendMessage(w + "...or simply use: " + g + "/ch CHANNEL_NAME");
		sender.sendMessage(g + "/ch credits " + w + "See who gave birth to this chat system!");
		sender.sendMessage(b + "==================================================");

		
	}
	
	private void CreateGuideBook(CommandSender sender){
		
		if (sender instanceof Player){
			
			Player p = (Player) sender;
			
			ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
			BookMeta bm = (BookMeta) book.getItemMeta();
			bm.setTitle(ChatColor.RED + "" + ChatColor.BOLD + "Channel Guide");
			bm.setAuthor(ChatColor.GREEN + "idru" + "m69");
			bm.setPages(this.pages);
			book.setItemMeta(bm);
			
			HashMap<Integer, ItemStack> leftovers = p.getInventory().addItem(book);
			if (leftovers.isEmpty()){
				
				sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "A channel Guide Book has been placed in your inventory!");
				
			} else {
				
				sender.sendMessage(Vars.Ntrl + "Oops. Make space in your inventory for the guide book!");
				
			}
			
		} else {
			
			sender.sendMessage(Vars.Ntrl + "Oops. Books can't be given to the console!");
			
		}
		
	}
}
