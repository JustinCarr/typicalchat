package org.jkjkkj.TypicalChat.Commands;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.Vars;

public class ToggleChatCMD implements CommandExecutor {
	
	private TCCommands local;
	
	public ToggleChatCMD(TCCommands inst){
		this.local = inst;
		
		this.local.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(this.local.plugin, new BukkitRunnable(){
			
			public void run(){
				
				if (!Vars.CHAT_ENABLED){
					String node = PermManager.getPerm("chatlocknotify");
					for (Player p : Bukkit.getServer().getOnlinePlayers()){
						
						if (p.hasPermission(node)){
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "NOTICE: Chat is currently " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "To re-enable it, use: " + Vars.Neg + "/togglechat on");
						}
					}
				}
			}
		}, 300L, 12000L);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("togglechat")){
			
			if (sender.hasPermission(PermManager.getPermNode("togglechat"))){
				
				
				if (args.length == 0){
					if (Vars.CHAT_ENABLED){
						Vars.ToggleChat(false);
						
						for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
							
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned " + Vars.Neg + "OFF" + Vars.Ntrl + "!");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may no longer chat or private message.");
						}
						if (!(sender instanceof Player)){
							ConsoleCommandSender css = this.local.plugin.getServer().getConsoleSender();
							css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned " + Vars.Neg + "OFF" + Vars.Ntrl + "!");
							css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may no longer chat or private message.");

						}
						
					} else {
						Vars.ToggleChat(true);
						
						for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned back " + Vars.Pos + "ON" + Vars.Ntrl + "!");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may continue to chat and send PMs!");
						}
						if (!(sender instanceof Player)){
							ConsoleCommandSender css = this.local.plugin.getServer().getConsoleSender();
							css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned " + Vars.Pos + "ON" + Vars.Ntrl + "!");
							css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may no longer chat or private message.");

						}
						
					}
				} else {
					
					if (args[0].equalsIgnoreCase("enable") || args[0].equalsIgnoreCase("on")){
						
						if (Vars.CHAT_ENABLED){
							sender.sendMessage(Vars.Ntrl + "Oops. Chat is already " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
						} else {
							Vars.ToggleChat(true);
							for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
								
								p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned " + Vars.Pos + "ON" + Vars.Ntrl + "!");
								p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may no longer chat or private message.");
							
							}
							if (!(sender instanceof Player)){
								ConsoleCommandSender css = this.local.plugin.getServer().getConsoleSender();
								css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned " + Vars.Pos + "ON" + Vars.Ntrl + "!");
								css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may no longer chat or private message.");

							}
						}
						
					} else if (args[0].equalsIgnoreCase("disable") || args[0].equalsIgnoreCase("off")){
						
						if (!Vars.CHAT_ENABLED){
							sender.sendMessage(Vars.Ntrl + "Oops. Chat is already " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
						} else {
							Vars.ToggleChat(false);
							for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
								
								p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned " + Vars.Neg + "OFF" + Vars.Ntrl + "!");
								p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may no longer chat or private message.");

							}
							if (!(sender instanceof Player)){
								ConsoleCommandSender css = this.local.plugin.getServer().getConsoleSender();
								css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Server-wide chat has been turned " + Vars.Pos + "ON" + Vars.Ntrl + "!");
								css.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may no longer chat or private message.");

							}
						}
						
					} else if (args[0].equalsIgnoreCase("check")) {
						
						if (Vars.CHAT_ENABLED){
							
							sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Currently the chat is " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
							
						} else {
							
							sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Currently the chat is " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
							
						}
						
					} else {
						
						sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: ");
						sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/togglechat on" + Vars.Ntrl + "\" - Turn chat ON");
						sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/togglechat off" + Vars.Ntrl + "\" - Turn chat OFF");
						sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/togglechat check" + Vars.Ntrl + "\" - Check the status of the chat");
						
					}

					
				}
				
			}
			return true;
			
		}
		
		return false;
	}
	
	
}
