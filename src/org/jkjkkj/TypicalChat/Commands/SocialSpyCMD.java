package org.jkjkkj.TypicalChat.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.Vars;

public class SocialSpyCMD implements CommandExecutor {

	
	public SocialSpyCMD(){
		
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (label.equalsIgnoreCase("socialspy")){
			
			if (sender.hasPermission(PermManager.getPerm("socialspy"))){
				
				if (!(sender instanceof Player)){
					sender.sendMessage(Vars.NOT_IN_CONSOLE);
					return true;
				}
				
				if (args.length == 0){
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try one of these:");
					sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/socialspy off" + Vars.Ntrl + "\" - to disable socialspy");
					sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/socialspy on" + Vars.Ntrl + "\" - to disable socialspy");
					sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/socialspy check" + Vars.Ntrl + "\" - to see if you're currently spying");
				} else if (args[0].equalsIgnoreCase("enable") || args[0].equalsIgnoreCase("on")) {
					
					Player p = (Player) sender;
					if (Vars.hasSocialSpyEnabled(p)){
						p.sendMessage(Vars.NOTICE_PREFIX + "You already have socialspy " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
					} else {
						Vars.modifySocialSpyDisabledList(p, false);
						p.sendMessage(Vars.NOTICE_PREFIX + "You now have socialspy " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
					}
					
				} else if (args[0].equalsIgnoreCase("disable") || args[0].equalsIgnoreCase("off")){
					
					Player p = (Player) sender;
					if (!Vars.hasSocialSpyEnabled(p)){
						p.sendMessage(Vars.NOTICE_PREFIX + "You already have socialspy " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
					} else {
						Vars.modifySocialSpyDisabledList(p, true);
						p.sendMessage(Vars.NOTICE_PREFIX + "You now have socialspy " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");
					}
					
				} else if (args[0].equalsIgnoreCase("check")){
					
					if (Vars.hasSocialSpyEnabled(((Player) sender))){
						sender.sendMessage(Vars.NOTICE_PREFIX + "You currently have socialspy " + Vars.Pos + "ENABLED" + Vars.Ntrl + "!");
						
					} else {
						sender.sendMessage(Vars.NOTICE_PREFIX + "You currently have socialspy " + Vars.Neg + "DISABLED" + Vars.Ntrl + "!");

					}
					
				} else {
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Arguments. Try one of these:");
					sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/socialspy off" + Vars.Ntrl + "\" - to disable socialspy");
					sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/socialspy on" + Vars.Ntrl + "\" - to disable socialspy");
					sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/socialspy check" + Vars.Ntrl + "\" - to see if you're currently spying");

				}
			}
			return true;
			
		}
		return false;
	}

	
	
}
