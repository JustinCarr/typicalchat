package org.jkjkkj.TypicalChat.Commands;

import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jkjkkj.TypicalChat.JoinLeaveHandler;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.UUIDManager;
import org.jkjkkj.TypicalChat.Vars;

public class IgnoreCMD implements CommandExecutor {

	public TCCommands local;
	private String b = ChatColor.AQUA + "";
	private String g = ChatColor.GOLD + "";
	private String w = ChatColor.WHITE + "";
	
	public IgnoreCMD(TCCommands instance){
		this.local = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("ignore")){
			
			if (!(sender instanceof Player)){
				sender.sendMessage(Vars.NOT_IN_CONSOLE);
				return true;
			}
			
			if (sender.hasPermission(PermManager.getPermNode("ignore"))){
				Player p = (Player) sender;
				
				if (args.length == 0){
					
					p.sendMessage(b + "==================================================");
					p.sendMessage(ChatColor.GRAY + "Here's how to use the '/ignore' command...");
					p.sendMessage(g + "/ignore help " + w + "View this help message");
					p.sendMessage(g + "/ignore list " + w + "See who you are currently ignoring");
					p.sendMessage(g + "/ignore add PLAYER " + w + "Begin ignoring a player");
					p.sendMessage(g + "/ignore remove PLAYER " + w + "Stop ignoring a player");
					p.sendMessage(b + "==================================================");
					
				} else if (args[0].equalsIgnoreCase("help")){
					
					p.sendMessage(b + "==================================================");
					p.sendMessage(ChatColor.GRAY + "Here's how to use the '/ignore' command...");
					p.sendMessage(g + "/ignore help " + w + "View this help message");
					p.sendMessage(g + "/ignore list " + w + "See who you are currently ignoring");
					p.sendMessage(g + "/ignore limit " + w + "Get the limit on ignored players");
					p.sendMessage(g + "/ignore add PLAYER " + w + "Begin ignoring a player");
					p.sendMessage(g + "/ignore remove PLAYER " + w + "Stop ignoring a player");
					p.sendMessage(b + "==================================================");
					
				} else if (args[0].equalsIgnoreCase("list")){
					
					p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Looking up who you're ignoring. Please wait...");
					
					List<String> ignored = Vars.getIgnoredAsStrings(p.getUniqueId());
					int size = ignored.size();
					
					if (ignored.isEmpty()){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are currently " + Vars.Neg + "not" + Vars.Ntrl + " ignoring anyone!");
					} else {
						
						
						StringBuilder sb = new StringBuilder();
						String comma = "";
						for (String pname : ignored){
							sb.append(comma).append(Vars.Neg + pname);
							comma = Vars.Ntrl + ", ";
						}
						
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are currently ignoring " + Vars.Pos + size + Vars.Ntrl + " players!");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "This includes: " + sb.toString().trim());
						
					}	
				} else if (args[0].equalsIgnoreCase("limit")){
					
					if (Vars.IGNORE_LIMIT == -1){
						
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You may ignore an " + Vars.Pos + "UNLIMITED" + Vars.Ntrl + " amount of players!");
						
					} else {
						
						List<UUID> ignored = Vars.getIgnoredAsUUIDS(p.getUniqueId());
						int size = ignored.size();
						
						if (ignored.isEmpty()){
							
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You can ignore a maximum of " + Vars.Pos + Vars.IGNORE_LIMIT + Vars.Ntrl + " players!");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "And... You're currently " + Vars.Neg + "NOT" + Vars.Ntrl + " ignoring anyone!");
							
						} else {
							
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You can ignore a maximum of " + Vars.Pos + Vars.IGNORE_LIMIT + Vars.Ntrl + " players!");
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "And... You're currently ignoring " + Vars.Pos + size + Vars.Ntrl + " player(s)!");
							
						}
						
					}
					
				} else if (args[0].equalsIgnoreCase("add")){
					
					if (args.length < 2){
						
						p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/ignore add PLAYER");
						return true;
						
					} 
					
					List<UUID> ignored = Vars.getIgnoredAsUUIDS(p.getUniqueId());
					if (ignored.size() >= Vars.IGNORE_LIMIT){
						
						if (!p.hasPermission(PermManager.getPerm("unlimitedignore"))){
							
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You cannot ignore another player as the max is " + Vars.Neg + Vars.IGNORE_LIMIT + Vars.Ntrl + "!");
							return true;
						}	
					} else if (args[1].equals("Server")){
						
						p.sendMessage(Vars.Ntrl + "Oops. The Server cannot be ignored!");
						return true;
						
					} else if (args[1].equals(p.getName())){
						
						p.sendMessage(Vars.Ntrl + "Oops. You cannot ignore yourself!");
						return true;
						
					}
					
					UUID uuid = null;
					
					if (JoinLeaveHandler.isValueOnline(args[1])){
						uuid = JoinLeaveHandler.getValueOnline(args[1]);
					} else {
						
						try {
							
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Please wait...");
							uuid = UUIDManager.getUUIDOf(args[1]);
							
						} catch (Exception e){
							
							uuid = null;
							
						}
						
					}
					
					if (uuid == null){
						
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + args[1] + " is not an existing minecraft player!");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Maybe they've recently changed their name to something new!");
						return true;
						
					} else if (ignored.contains(uuid)){
						
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are already ignoring " + Vars.Neg + args[1] + Vars.Ntrl + "!");
					
					} else {
						
						Vars.setIgnored(p.getUniqueId(), uuid);
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are now ignoring " + Vars.Pos + args[1] + Vars.Ntrl + "!");
					
					}
					
				} else if (args[0].equalsIgnoreCase("remove")){
					
					if (args.length < 2){
						
						p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/ignore remove PLAYER");
						return true;
						
					}
					
					List<UUID> ignored = Vars.getIgnoredAsUUIDS(p.getUniqueId());
					if (args[1].equals("Server")){
						
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You already aren't ignoring " + Vars.Neg + "Server" + Vars.Ntrl + "!");
						return true;
						
					} else if (args[1].equals(p.getName())){
						
						p.sendMessage(Vars.Ntrl + "Oops. You couldn't even ignore yourself in the first place!");
						return true;
						
					} else if (ignored.isEmpty()){
						
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You already aren't ignoring " + Vars.Neg + args[1] + Vars.Ntrl + "!");
						return true;
						
					}
					
					UUID uuid = null;
					
					if (JoinLeaveHandler.isValueOnline(args[1])){
						uuid = JoinLeaveHandler.getValueOnline(args[1]);
					} else {
						
						try {
							
							p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Please wait...");
							uuid = UUIDManager.getUUIDOf(args[1]);
							
						} catch (Exception e){
							
							uuid = null;
							
						}
						
					}
					
					if (uuid == null){
						
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + args[1] + " is not an existing minecraft player!");
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Maybe they've recently changed their name to something new!");
						return true;
						
					} else if (!ignored.contains(uuid)){
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You already aren't ignoring " + Vars.Neg + args[1] + Vars.Ntrl + "!");
					} else {
						
						Vars.removeIgnored(p.getUniqueId(), uuid);
						p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "You are no longer ignoring " + Vars.Pos + args[1] + Vars.Ntrl + "!");
					}
					
				} else {
					
					p.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. For help: " + Vars.Neg + "/ignore help");
					
				}
				
				
				/*
				 * 847.990.5268
				 * Hey, my name's Justin, I'm a student at warren township highschool..
				 * lookin to do some volunteer work, can I get details here?
				 * 
				 * 
				 * 
				 * */
				
				
			}
			
			return true;
		}
		
		return false;
	}
	
}
