package org.jkjkkj.TypicalChat.Commands;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.Vars;

public class StaffVotesLoveCMD implements CommandExecutor {

	private TCCommands local;
	private static Chat chat = null;
	
	public StaffVotesLoveCMD(TCCommands inst){
		
		this.local = inst;
		setupChat();
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (label.equalsIgnoreCase("staff")){
			
			if (sender.hasPermission(PermManager.getPermNode("staff"))){
				
				if (!(sender instanceof Player)){
					sender.sendMessage(ChatColor.RED + "This can only be done in-game!");
					return true;
				}
				
				Player p = (Player) sender;
				setupChat();
				
				String tosend = Vars.STAFF.replaceAll("%SENDER%", ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(p) + p.getName()));
				
				for (Player p1 : this.local.plugin.getServer().getOnlinePlayers()){
					p1.sendMessage(tosend);
				}	
				
			}
			return true;
		} else if (label.equalsIgnoreCase("votes")){
			
			if (sender.hasPermission(PermManager.getPermNode("votes"))){
				
				for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
					p.sendMessage(Vars.VOTES);

				}	
				
			}
			return true;
		} else if (label.equalsIgnoreCase("love")){
			if (sender.hasPermission(PermManager.getPermNode("love"))){
				
				if (!(sender instanceof Player)){
					sender.sendMessage(Vars.NOT_IN_CONSOLE);
					return true;
				}
				
				String tosend = Vars.TC.replaceAll("%SENDER%", sender.getName());
				for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
					p.sendMessage(tosend);
				}
				
			}
			return true;
		}
		
		return false;
	}
	
	private boolean setupChat(){
        RegisteredServiceProvider<Chat> chatProvider = this.local.plugin.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

}
