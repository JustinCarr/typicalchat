package org.jkjkkj.TypicalChat.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jkjkkj.TypicalChat.JoinLeaveHandler;
import org.jkjkkj.TypicalChat.PermManager;
import org.jkjkkj.TypicalChat.UsefulUtils;
import org.jkjkkj.TypicalChat.Vars;

public class ClearChatCMD implements CommandExecutor {

	private TCCommands local;
	
	public ClearChatCMD(TCCommands inst){
		this.local = inst;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (sender.hasPermission(PermManager.getPermNode("clearchat"))){
							
			if (args.length == 0){
				
				sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try one of these: ");
				sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/clearchat now" + Vars.Ntrl + "\"" + Vars.Ntrl + " - Clear chat now");
				sender.sendMessage(Vars.Ntrl + "\"" + Vars.Neg + "/clearchat in SECONDS" + Vars.Ntrl + "\"" + Vars.Ntrl + " - Clear chat later. SECONDS must more than 0, less than 30.");
				
			} else if (args[0].equalsIgnoreCase("now")){
				
				String n = "";
				Boolean toput = true;
				if (sender instanceof Player){
					if (UsefulUtils.IsVanished(sender.getName())){
						toput = false;
					}
					n = sender.getName();
				} else {
					
					n = "INTERNAL SERVER";
					
				}
				
				sender.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Global chat has been cleared.");
				
				for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
					
					if (p.getName().equals(n)){continue;}
					
					for (int i = 0; i < 93; i++){
						
						p.sendMessage("");
						
					}
					
					p.sendMessage(Vars.Ntrl + "==================================================");
					p.sendMessage("");
					if (toput){
						p.sendMessage(Vars.Neg + "CHAT HAS BEEN CLEARED BY " + n + "!");
					} else {
						p.sendMessage(Vars.Neg + "CHAT HAS BEEN CLEARED!");
					}
					p.sendMessage(Vars.Ntrl + ChatColor.ITALIC + "Sorry for the inconvenience!");
					p.sendMessage("");
					p.sendMessage(Vars.Ntrl + "==================================================");
					p.sendMessage("");
					
				}
			} else if (args[0].equalsIgnoreCase("in") || args[0].equalsIgnoreCase("later")){
				
				if (args.length != 2){
					
					sender.sendMessage(Vars.Ntrl + "Oops. Incorrect Usage. Try: " + Vars.Neg + "/clearchat in SECONDS");
					return true;
				}
				
				int i = -1;
				
				try {
					
					i = Integer.parseInt(args[1]);
					
				} catch (Exception e){
					
					sender.sendMessage(Vars.Ntrl + "Oops. 'SECONDS' argument must be a number! Ex. 1, 2,...");
					return true;
					
				}
				
				if (i > 30){
					
					sender.sendMessage(Vars.Ntrl + "Oops. The 'SECONDS' argument cannot be greater than 30!");
					return true;
					
				} else if (i <= 0){
					
					sender.sendMessage(Vars.Ntrl + "Oops. The 'SECONDS' argument cannot be less than 1!");
					return true;
					
				}
				
				final String player;
				
				if (sender instanceof Player){
					player = sender.getName();
				} else {
					player = "Server";
				}
				
				for (Player p : this.local.plugin.getServer().getOnlinePlayers()){
					
					p.sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "WARNING: Chat will be cleared in " + Vars.Neg + i + Vars.Ntrl + " seconds!");
					
				}
				
				this.local.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.local.plugin, new BukkitRunnable(){
					
					public void run(){
						
						clearTheChat(player);
						
					}
					
				}, (i * 20));
				
			}
			
			return true;
		}

		return false;
	}
	
	public static void clearTheChat(String pinit){
		
		String n = "";
		Boolean toput = true;
		if (pinit.equals("Server")){
			
			n = "INTERNAL SERVER";
			Bukkit.getConsoleSender().sendMessage(Vars.NOTICE_PREFIX + Vars.Ntrl + "Global chat has been cleared.");

		} else {
						
			if (JoinLeaveHandler.isValueOnline(pinit)){
				if (UsefulUtils.IsVanished(pinit)){
					toput = false;
				}
			}
			n = pinit;
			
		}
				
		for (Player p : Bukkit.getServer().getOnlinePlayers()){
			
			
			for (int i = 0; i < 93; i++){
				
				p.sendMessage("");
				
			}
			
			p.sendMessage(Vars.Ntrl + "==================================================");
			p.sendMessage("");
			if (toput){
				p.sendMessage(Vars.Neg + "CHAT HAS BEEN CLEARED BY " + n + "!");
			} else {
				p.sendMessage(Vars.Neg + "CHAT HAS BEEN CLEARED!");
			}
			p.sendMessage(Vars.Ntrl + ChatColor.ITALIC + "Sorry for the inconvenience!");
			p.sendMessage("");
			p.sendMessage(Vars.Ntrl + "==================================================");
			p.sendMessage("");
			
		}
		
	}

}
